VERSION 5.00
Begin VB.Form FrmConformar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Conformaci�n de Denominaci�n"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5550
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   5550
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox TxtTelefono 
      Height          =   360
      Left            =   2280
      TabIndex        =   2
      Top             =   2174
      Width           =   2250
   End
   Begin VB.TextBox TxtIdentificacion 
      Height          =   360
      Left            =   2280
      TabIndex        =   1
      Top             =   1747
      Width           =   2250
   End
   Begin VB.TextBox TxtNombre 
      Height          =   360
      Left            =   2280
      TabIndex        =   0
      Top             =   1320
      Width           =   2250
   End
   Begin VB.TextBox TxtClave 
      Height          =   360
      Left            =   2280
      TabIndex        =   4
      Top             =   3030
      Width           =   2250
   End
   Begin VB.TextBox TxtOperador 
      Height          =   360
      Left            =   2280
      TabIndex        =   3
      Top             =   2601
      Width           =   2250
   End
   Begin VB.CommandButton aceptar 
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3015
      Picture         =   "FrmConformar.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3525
      Width           =   1170
   End
   Begin VB.CommandButton cancelar 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   4260
      Picture         =   "FrmConformar.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3525
      Width           =   1170
   End
   Begin VB.Image Bteclado 
      Height          =   420
      Left            =   4575
      Picture         =   "FrmConformar.frx":1994
      Stretch         =   -1  'True
      Top             =   1320
      Width           =   930
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tel�fono N�"
      Height          =   240
      Left            =   135
      TabIndex        =   17
      Top             =   2220
      Width           =   1170
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Identificaci�n N�"
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   1800
      Width           =   1650
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre del cliente"
      Height          =   240
      Left            =   135
      TabIndex        =   15
      Top             =   1365
      Width           =   1830
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Clave"
      Height          =   240
      Left            =   135
      TabIndex        =   14
      Top             =   3090
      Width           =   540
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Operador N�/Nombre"
      Height          =   240
      Left            =   135
      TabIndex        =   13
      Top             =   2655
      Width           =   2040
   End
   Begin VB.Label LblMonto 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   240
      Left            =   1890
      TabIndex        =   12
      Top             =   855
      Width           =   3570
   End
   Begin VB.Label LblCuenta 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "##############"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   240
      Left            =   1890
      TabIndex        =   11
      Top             =   465
      Width           =   3570
   End
   Begin VB.Label LblEmisor 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "##############"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   240
      Left            =   1890
      TabIndex        =   10
      Top             =   90
      Width           =   3570
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Monto"
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   855
      Width           =   615
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Cuenta Numero"
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   465
      Width           =   1515
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Entidad Emisora"
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   90
      Width           =   1560
   End
End
Attribute VB_Name = "FrmConformar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public FMonto As Double, FDecimales As Long, FCuentaNo As String, FEmisor As String
Public FOperadorConformacion As String, FClaveConformacion As String
Public FNombreCliente As String, FIdentificacionCliente As String, FTelefonoCliente As String
Public ClaseBancos As New Cls_Bancos
Public ConexionPosLocal As Object
Public ConexionAdmLocal As Object
Public IgnorarProximo As Boolean

Private ClaseReportes As New Cls_Reportes
Private ClaseRutinas As New cls_Rutinas
Private ClaseDispositivos As New clsDispositivos
Private NumeroDispositivo As Long
Private EsteDispositivo As Dispositivos
Private TxtObjeto As Object
Private BotonCancelar As Variant

Private Sub aceptar_Click()
    On Error GoTo FALLA_LOCAL
    EsteDispositivo = POS.Propiedades.MisDispositivos(NumeroDispositivo)
    If Not EsteDispositivo.OComApuntador Is Nothing Then
        'IMPRIMIR POR PUERTOS SERIALES
    ElseIf Not EsteDispositivo.OOPOSApuntador Is Nothing Then
        'IMPRIMIR POR OPOS
    Else
        'IMPRIMIR POR LPT
        'Call ClaseBancos.imp_DatosConformacionCheque_Standard_Slip(Me.TxtNombre, Me.TxtTelefono, Me.TxtIdentificacion, Me.txtClave, Me.TxtOperador, POS.UsuarioDelPos.Descripcion, POS.POSNo)
        If Trim(POS.ImpresoraCheque) = "" Then
            Call ClaseReportes.imp_DatosConformacionCheque_Standard_SlipRtf(Me.TxtNombre, Me.TxtTelefono, Me.TxtIdentificacion, Me.TxtClave, Me.TxtOperador, POS.UsuarioDelPos.Descripcion, POS.POSNo)
        Else
            Call ClaseReportes.imp_DatosConformacionCheque_Standard_SlipRtf(Me.TxtNombre, Me.TxtTelefono, Me.TxtIdentificacion, Me.TxtClave, Me.TxtOperador, POS.UsuarioDelPos.Descripcion, POS.POSNo, POS.ImpresoraCheque, POS.TipoPuertoImpresoraCheque)
        End If
    End If
    FOperadorConformacion = Me.TxtOperador.Text
    FClaveConformacion = Me.TxtClave.Text
    FNombreCliente = Me.TxtNombre.Text
    FTelefonoCliente = Me.TxtTelefono.Text
    FIdentificacionCliente = Me.TxtIdentificacion.Text
    Unload Me
    Exit Sub
FALLA_LOCAL:
    ClaseRutinas.Mensajes Err.Description, False
    Unload Me
End Sub

Private Sub Aceptar_GotFocus()
    Set TxtObjeto = Nothing
End Sub

Private Sub Bteclado_Click()
    If Not TxtObjeto Is Nothing Then
        ClaseRutinas.LlamarTeclado TxtObjeto
    End If
End Sub

Private Sub cancelar_Click()
    If PosClass.FunctionTeclasAutorizar(-2, 0, POS.POSNo, CInt(POS.UsuarioDelPos.Nivel), POS.Branch, ConexionAdmLocal, ConexionPosLocal, POS.Forma) Then
        If ClaseRutinas.Mensajes("�Desea ignorar las pr�ximas solicitudes?", True) Then
            IgnorarProximo = True
        End If
        Unload Me
    End If
End Sub

Private Sub Cancelar_GotFocus()
    Set TxtObjeto = Nothing
End Sub

Private Sub Form_Activate()
    If ConexionPosLocal Is Nothing Then
        GRutinas.Mensajes "No se pudo hacer referencia a una conexi�n...!", False
        End
    End If
    If FMonto <= 0 Then
        ClaseRutinas.Mensajes "No se especific� un monto para esta denominaci�n.", False
        Unload Me
    End If
    BotonCancelar = PosClass.NivelDeTecla(-2, 0, POS.POSNo, POS.Branch, ConexionPosLocal)
    If Not IsEmpty(BotonCancelar) Then
        If UBound(BotonCancelar) >= 10 Then
            Me.cancelar.Enabled = BotonCancelar(5)
        Else
            Me.cancelar.Enabled = False
        End If
    Else
        Me.cancelar.Enabled = False
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyReturn Then
        ClaseRutinas.SendKeys Chr(vbKeyTab), False
    ElseIf Shift = 0 And (KeyCode = vbKeyEscape Or KeyCode = vbKeyF12) Then
        Call cancelar_Click
    End If
End Sub

Private Sub Form_Load()
    Me.LblMonto.Caption = FormatNumber(FMonto, FDecimales)
    Me.LblCuenta.Caption = FCuentaNo
    Me.LblEmisor.Caption = FEmisor
    Me.TxtClave.Text = FClaveConformacion
    Me.TxtOperador.Text = FOperadorConformacion
    Me.TxtIdentificacion.Text = FIdentificacionCliente
    Me.TxtNombre.Text = FNombreCliente
    Me.TxtTelefono.Text = FTelefonoCliente
    NumeroDispositivo = ClaseDispositivos.SelectDispositivo("IMPRESORA")
End Sub

Private Sub txtClave_GotFocus()
    Set TxtObjeto = TxtClave
    ClaseRutinas.SeguirText TxtObjeto, Bteclado
End Sub

Private Sub TxtIdentificacion_GotFocus()
    Set TxtObjeto = TxtIdentificacion
    ClaseRutinas.SeguirText TxtObjeto, Bteclado
End Sub

Private Sub TxtNombre_GotFocus()
    Set TxtObjeto = TxtNombre
    ClaseRutinas.SeguirText TxtObjeto, Bteclado
End Sub

Private Sub TxtOperador_GotFocus()
    Set TxtObjeto = TxtOperador
    ClaseRutinas.SeguirText TxtObjeto, Bteclado
End Sub
Private Sub TxtTelefono_GotFocus()
    Set TxtObjeto = TxtTelefono
    ClaseRutinas.SeguirText TxtObjeto, Bteclado
End Sub
