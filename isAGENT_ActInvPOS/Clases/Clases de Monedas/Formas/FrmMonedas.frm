VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form FrmMonedas 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   2190
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   3150
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   14.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2190
   ScaleWidth      =   3150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox TxtBusqueda 
      Height          =   465
      Left            =   1065
      TabIndex        =   1
      Top             =   1725
      Width           =   2040
   End
   Begin MSComctlLib.ListView MONEDAS 
      Height          =   1710
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3150
      _ExtentX        =   5556
      _ExtentY        =   3016
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      OLEDragMode     =   1
      OLEDropMode     =   1
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
      NumItems        =   0
   End
   Begin VB.Image Bteclado 
      BorderStyle     =   1  'Fixed Single
      Height          =   450
      Left            =   15
      Picture         =   "FrmMonedas.frx":0000
      Stretch         =   -1  'True
      Top             =   1725
      Width           =   1050
   End
End
Attribute VB_Name = "FrmMonedas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Clasemonedas As New cls_Monedas
Public ConexionAdm As Object
Public ConexionPos As Object
Private RsMonedas As Object
Private Rutinas As New cls_Rutinas
Private Const MinLen As Long = 3000
Private Sub Bteclado_Click()
    TxtBusqueda.SetFocus
    Rutinas.LlamarTeclado TxtBusqueda
End Sub

Private Sub Form_Activate()
    Clasemonedas.BuscarMonedas RsMonedas
    Call ConfigReportView(RsMonedas)
    If MONEDAS.ListItems.Count < 2 Then
        Unload Me
    End If
    TxtBusqueda.Width = MONEDAS.Width - TxtBusqueda.Left - 100
End Sub

Private Sub MONEDAS_DblClick()
    Clasemonedas.CodMoneda = FrmMonedas.MONEDAS.SelectedItem.Text
    Clasemonedas.DesMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(1).Text
    Clasemonedas.FacMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(2).Text
    Clasemonedas.PrefMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(3).Text
    Clasemonedas.ActMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(4).Text
    Clasemonedas.SimMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(5).Text
    Clasemonedas.DecMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(6).Text
    Me.Visible = False
End Sub

Private Sub MONEDAS_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And Shift = 0 Then
        Clasemonedas.CodMoneda = FrmMonedas.MONEDAS.SelectedItem.Text
        Clasemonedas.DesMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(1).Text
        Clasemonedas.FacMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(2).Text
        Clasemonedas.PrefMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(3).Text
        Clasemonedas.ActMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(4).Text
        Clasemonedas.SimMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(5).Text
        Clasemonedas.DecMoneda = FrmMonedas.MONEDAS.SelectedItem.ListSubItems(6).Text
        Me.Visible = False
    End If
End Sub

Private Sub Form_Load()
    Clasemonedas.InicializarConexiones ConexionAdm, ConexionPos
End Sub

Private Sub ConfigReportView(Registros As Object)
    Dim ActItem As ListItem, Ancho As Integer
    MONEDAS.Width = 0
    MONEDAS.Left = 0
    MONEDAS.ColumnHeaders.Add 1, , "CODIGO", 0
    MONEDAS.ColumnHeaders.Add 2, , "DESCRIPCION", MONEDAS.Width
    MONEDAS.ColumnHeaders.Add 3, , "FACTOR", 0
    MONEDAS.ColumnHeaders.Add 4, , "PREFERENCIA", 0
    MONEDAS.ColumnHeaders.Add 5, , "ACTIVA", 0
    MONEDAS.ColumnHeaders.Add 6, , "SIMBOLO", 0
    MONEDAS.ColumnHeaders.Add 7, , "DECIMALES", 0
    While Not Registros.EOF
    
        If Clasemonedas.TieneDenominaciones(Registros!C_CODMONEDA) Then
            Set ActItem = MONEDAS.ListItems.Add(, , Registros!C_CODMONEDA)
            ActItem.SubItems(1) = Registros!c_descripcion
            
            Ancho = Rutinas.CalcularAncho(Registros!c_descripcion, Me)
            If CLng(MONEDAS.Width) < CLng(Ancho) Then
                MONEDAS.Width = Ancho
                MONEDAS.ColumnHeaders(2).Width = Ancho
                Me.Width = Ancho
            End If
            If MONEDAS.Width < MinLen Then
                MONEDAS.Width = MinLen
                MONEDAS.ColumnHeaders(2).Width = MinLen
                Me.Width = MinLen
            End If
            
            ActItem.SubItems(2) = Registros!N_FACTOR
            ActItem.SubItems(3) = Registros!b_preferencia
            ActItem.SubItems(4) = Registros!b_activa
            ActItem.SubItems(5) = Registros!c_simbolo
            ActItem.SubItems(6) = Registros!n_decimales
        End If
        Registros.MoveNext
    Wend
End Sub

Public Sub BuscarEnLista(Listado As ListView, Palabra As String, Optional DarFoco As Boolean = True)
    Dim LCont As Long, DondeEsta As Long
    For LCont = 1 To Listado.ListItems.Count
         DondeEsta = InStr(1, UCase(Mid(Listado.ListItems(LCont).ListSubItems(1).Text, 1, Len(Palabra))), UCase(Palabra))
         If DondeEsta > 0 Then
            If DarFoco Then
                Listado.SetFocus
            End If
            Listado.ListItems(LCont).Selected = True
            Listado.ListItems(LCont).EnsureVisible
            Exit Sub
         End If
    Next LCont
End Sub

Private Sub TxtBusqueda_Change()
    Call BuscarEnLista(MONEDAS, TxtBusqueda.Text, False)
End Sub

Private Sub TxtBusqueda_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyReturn Then
        Call BuscarEnLista(MONEDAS, TxtBusqueda.Text)
    End If
End Sub

