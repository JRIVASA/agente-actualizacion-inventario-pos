VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{A0AD77AC-9E22-49A2-93ED-4F1729AD8BDE}#33.0#0"; "S_GRID_.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{EC7E5EE7-6BE1-4524-8E54-E893653F621F}#17.0#0"; "S_Texto.ocx"
Begin VB.Form FrmPagos 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   11910
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8205
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin S_Texto.S_text Datos 
      Height          =   360
      Left            =   1635
      TabIndex        =   32
      Top             =   4620
      Visible         =   0   'False
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   635
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxDate         =   2958465
      MinDate         =   -109205
      Text            =   ""
   End
   Begin S_GRID_.S_GRID FormasPagos 
      Height          =   3675
      Left            =   15
      TabIndex        =   0
      Top             =   4500
      Width           =   11790
      _ExtentX        =   20796
      _ExtentY        =   6482
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorBkg    =   -2147483633
      BackColorFixed  =   -2147483633
      BackColorSel    =   -2147483635
      BackColorUnpopulated=   -2147483633
      Sp_AutoIncrementar=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorFixed  =   -2147483633
      BackColorSel    =   -2147483635
      BackColorBkg    =   -2147483633
      BackColorUnpopulated=   -2147483633
      ColAlignment0   =   9
      ColWidth0       =   -1
      ForeColorSel    =   -2147483634
      ForeColorFixed  =   -2147483630
      GridColor       =   -2147483630
      GridLineWidth   =   2
      GridLineWidthBand0=   2
      GridLineWidthFixed=   2
      GridLineWidthHeader0=   2
      GridLineWidthIndent0=   2
      GridLineWidthUnpopulated=   2
      RowHeight0      =   240
      sp_Autotab      =   0   'False
   End
   Begin VB.Frame FrameBotones 
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Left            =   30
      TabIndex        =   31
      Top             =   -60
      Width           =   11880
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   60
         TabIndex        =   14
         Top             =   135
         Width           =   11700
         _ExtentX        =   20638
         _ExtentY        =   1429
         ButtonWidth     =   2249
         ButtonHeight    =   1376
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         ImageList       =   "BarraMenu"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   9
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Eliminar"
               Key             =   "Eliminar"
               Object.ToolTipText     =   "F7 Eliminar"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "F4 Grabar"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Buscar"
               Key             =   "Buscar"
               Object.ToolTipText     =   "F2 Buscar"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Imprimir &Forma"
               Key             =   "forma"
               Object.ToolTipText     =   "Ctrl - F6 Imprimir Forma de pago"
               ImageIndex      =   11
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Conformaci�n"
               Key             =   "conformar"
               Object.ToolTipText     =   "Alt - F6 Conformaci�n"
               ImageIndex      =   14
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "F12 Salir"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               Object.ToolTipText     =   "F1 Ayuda"
               ImageIndex      =   2
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00FFFFFF&
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3465
      Left            =   30
      TabIndex        =   22
      Top             =   990
      Width           =   3615
      Begin VB.Label Lblcargo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Cargo por servicio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   270
         Left            =   135
         TabIndex        =   34
         Top             =   1380
         Width           =   2385
      End
      Begin VB.Label servicio 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   135
         TabIndex        =   33
         Top             =   1620
         Width           =   3285
      End
      Begin VB.Label subtotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   465
         Left            =   135
         TabIndex        =   30
         Top             =   420
         Width           =   3285
      End
      Begin VB.Label impuesto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   135
         TabIndex        =   29
         Top             =   2250
         Width           =   3285
      End
      Begin VB.Label lblimpuesto 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Impuesto"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   270
         Left            =   135
         TabIndex        =   28
         Top             =   2055
         Width           =   1260
      End
      Begin VB.Label lblsubtotal 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "SubTotal"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   270
         Left            =   135
         TabIndex        =   27
         Top             =   225
         Width           =   1170
      End
      Begin VB.Label totalb 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   405
         Left            =   135
         TabIndex        =   26
         Top             =   2850
         Width           =   3285
      End
      Begin VB.Label LBLTOTAL 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   270
         Left            =   135
         TabIndex        =   25
         Top             =   2685
         Width           =   675
      End
      Begin VB.Label LblDescuento 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Descuento"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   270
         Left            =   135
         TabIndex        =   24
         Top             =   780
         Width           =   1395
      End
      Begin VB.Label Descuento 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   135
         TabIndex        =   23
         Top             =   1065
         Width           =   3285
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3465
      Left            =   3690
      TabIndex        =   17
      Top             =   990
      Width           =   5235
      Begin VB.Label lbCancelado 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Cancelado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   645
         Left            =   90
         TabIndex        =   21
         Top             =   225
         Width           =   5070
      End
      Begin VB.Label lbCAMBIO 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Cambio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   585
         Left            =   90
         TabIndex        =   20
         Top             =   1695
         Width           =   5070
      End
      Begin VB.Label Cancel 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   780
         Left            =   90
         TabIndex        =   19
         Top             =   915
         Width           =   5070
      End
      Begin VB.Label vuelto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   750
         Left            =   90
         TabIndex        =   18
         Top             =   2445
         Width           =   5070
      End
   End
   Begin VB.Frame FrmNumeros 
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3465
      Left            =   8985
      TabIndex        =   15
      Top             =   990
      Width           =   2925
      Begin VB.CommandButton btn_teclado 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   4
         Left            =   60
         TabIndex        =   5
         Top             =   795
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   5
         Left            =   780
         TabIndex        =   6
         Top             =   795
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   6
         Left            =   1500
         TabIndex        =   7
         Top             =   795
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   7
         Left            =   60
         TabIndex        =   8
         Top             =   285
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   8
         Left            =   780
         TabIndex        =   9
         Top             =   285
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   9
         Left            =   1500
         TabIndex        =   10
         Top             =   285
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   0
         Left            =   60
         TabIndex        =   1
         Top             =   1815
         Width           =   1395
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   1
         Left            =   60
         TabIndex        =   2
         Top             =   1305
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   2
         Left            =   780
         TabIndex        =   3
         Top             =   1305
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   3
         Left            =   1500
         TabIndex        =   4
         Top             =   1305
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   10
         Left            =   1500
         TabIndex        =   16
         Top             =   1815
         Width           =   675
      End
      Begin VB.CommandButton btn_teclado 
         BackColor       =   &H00C0E0FF&
         Caption         =   "<-----"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   11
         Left            =   2220
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   285
         Width           =   645
      End
      Begin VB.CommandButton btn_teclado 
         BackColor       =   &H80000018&
         Caption         =   "Enter"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   12
         Left            =   2220
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1305
         Width           =   645
      End
      Begin VB.CommandButton btn_teclado 
         BackColor       =   &H00C0E0FF&
         Caption         =   "CLS"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   13
         Left            =   2220
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   795
         Width           =   645
      End
   End
   Begin MSWinsockLib.Winsock sock 
      Left            =   0
      Top             =   330
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList BarraMenu 
      Left            =   600
      Top             =   420
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   15
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":0000
            Key             =   "ATRAS"
            Object.Tag             =   "ATRAS"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":0CDA
            Key             =   "AYUDA"
            Object.Tag             =   "AYUDA"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":19B4
            Key             =   "BORRAR"
            Object.Tag             =   "BORRAR"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":268E
            Key             =   "BUSCAR"
            Object.Tag             =   "BUSCAR"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":3368
            Key             =   "CANCELAR"
            Object.Tag             =   "CANCELAR"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":4042
            Key             =   "DEPOSITO"
            Object.Tag             =   "DEPOSITO"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":4D1C
            Key             =   "ESTADISTICAS"
            Object.Tag             =   "ESTADISTICAS"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":59F6
            Key             =   "GRABAR"
            Object.Tag             =   "GRABAR"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":76D0
            Key             =   "SALIR"
            Object.Tag             =   "SALIR"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":83AA
            Key             =   "ACEPTAR"
            Object.Tag             =   "ACEPTAR"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":9084
            Key             =   "IMPRIMIR"
            Object.Tag             =   "IMPRIMIR"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":9D5E
            Key             =   "CARPETA"
            Object.Tag             =   "CARPETA"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":AA38
            Key             =   "INFORMACION"
            Object.Tag             =   "INFORMACION"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":B712
            Key             =   "CONFORMAR"
            Object.Tag             =   "CONFORMAR"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPagos.frx":D3EC
            Key             =   "CUENTAS"
            Object.Tag             =   "CUENTAS"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmPagos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ConexionAdm As Object
Public ConexionPos As Object
Public ConexionPosLocal As Object
Public ConexionAdmLocal As Object
Public RegionCritical As Boolean

Public Clasemonedas As New cls_Monedas
Public ClasePagos As New cls_Pagos
Public Rutinas As New cls_Rutinas
Public ClaseMeseros As New cls_mesoneros
Public ClaseDenominaciones As New cls_Denominaciones
Public ClaseCliente As New clsDatosCliente
Private ClaseMesas As New clsMesas

Public MonedaFacturar As String
Public MonedaFormas As String
Public CuentaNumero As String, ServicioNumero As String, Relacion As String, Mesero As String, CodigoCliente As String
Public Organizacion As String, Localidad As String, Caja As String, Turno As Long, Usuario As String
Public isPosFood As Boolean, GrabarOk As Boolean, SaltarConformar As Boolean, SaltarImprimir As Boolean

Private RsMonedas As Object
Private RsMonedaFacturar As Object
Private RsBancos As Object
Private RsDenominaciones As Object
Private RsCuenta As Object

Private MontoVuelto As Double
Private UltimaTecla As Integer
Private FormaCargada As Boolean

Private MontoComision As Double
Private FTotal As Double
Private FSubtotal As Double, FDescuento As Double, FImpuestos As Double

Private FTotalComision As Double
Private FSubtotalComision As Double, FDescuentoComision As Double, FImpuestosComision As Double

Private FTotalSinComision As Double
Private FSubtotalSinComision As Double, FDescuentoSinComision As Double, FImpuestosSinComision As Double


Private MontoCredito As Double
Private TxtMontoProcesar As Object

Private NombreCliente As String
Private Identificacion As String
Private Telefono As String

Private CuentasAUnir As Variant                 'ARREGLO QUE CONTIENE LAS CUENTAS
Private ResetearSiSalir As Boolean

Private Sub btn_teclado_Click(Index As Integer)
    Dim PosAct As Variant
    PosAct = FormasPagos.sp_CeldaActual
    Select Case Index
        Case 0 To 9
            Call ClasePagos.ProcesarTeclaSobreColumna(FormasPagos, Datos, CInt(PosAct(0)), CInt(PosAct(1)), Asc(btn_teclado(Index).Caption))
        Case 10
            Call ClasePagos.ProcesarTeclaSobreColumna(FormasPagos, Datos, CInt(PosAct(0)), CInt(PosAct(1)), Asc(Rutinas.SDecimal))
        Case 11 'BKSP
            Call ClasePagos.ProcesarTeclaSobreColumna(FormasPagos, Datos, CInt(PosAct(0)), CInt(PosAct(1)), vbKeyBack)
        Case 12 'ENTER
            Call ClasePagos.ProcesarTeclaSobreColumna(FormasPagos, Datos, CInt(PosAct(0)), CInt(PosAct(1)), vbKeyReturn)
            Call FormasPagos_LeaveCell
        Case 13 'CLS
            Call ClasePagos.ProcesarTeclaSobreColumna(FormasPagos, Datos, CInt(PosAct(0)), CInt(PosAct(1)), vbKeyDelete)
    End Select
    If FormasPagos.Enabled Then
        FormasPagos.SetFocus
    End If
End Sub

Private Sub Datos_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim PosAct As Variant
    PosAct = FormasPagos.sp_CeldaActual
    Select Case Shift
        Case vbAltMask
            Select Case KeyCode
            End Select
        Case vbCtrlMask
            Select Case KeyCode
            End Select
        Case Else
            Select Case KeyCode
                Case vbKeyEscape
                    Datos.Visible = False
                    FormasPagos.Enabled = True
                Case vbKeyReturn
                    Call ClasePagos.ProcesarTeclas(FormasPagos, Datos, RsMonedaFacturar!N_FACTOR, CInt(PosAct(1)), MontoVuelto, Me.Left + FormasPagos.CellLeft + FormasPagos.Left, Me.Top + FormasPagos.CellTop + FormasPagos.Top, vbKeyReturn)
                    Datos.Visible = False
                    Call CalcularNuevoMonto
            End Select
    End Select
    Call FormasPagos_LeaveCell
End Sub

Private Sub Form_Activate()
On Error GoTo Falla_Forma
    If Not FormaCargada Then
    
        GrabarOk = False
        SaltarConformar = False
        SaltarImprimir = False
        Set TxtMontoProcesar = totalB
        
'INICIALIZAMOS LAS CLASES
'*****************************************************************************************
        Clasemonedas.InicializarConexiones ConexionAdm, ConexionPos
        ClasePagos.InicializarConexiones ConexionAdm, ConexionPos, ConexionAdmLocal, ConexionPosLocal
        ClaseMeseros.InicializarConexiones ConexionAdm, ConexionPos
        ClaseCliente.InicializarConexiones ConexionAdm, ConexionPos
'*****************************************************************************************
'BUSCAMOS LAS MONEDAS QUE SE USA PARA FACTURAR
        Clasemonedas.BuscarMonedas RsMonedaFacturar, MonedaFacturar, , True
        
        If RsMonedaFacturar.EOF Then
            Rutinas.Mensajes "Moneda a facturar no encontrada.", False
            Unload Me
            Exit Sub
        End If

'CARAMOS LOS DATOS DEL MESONERO PARA BUSCAR SUS COMISIONES
        
        If Not ClaseMeseros.DatosDeCuenta(CodigoCliente, CuentaNumero, ServicioNumero, Relacion, RsCuenta) Then
            Rutinas.Mensajes "No se encuentran datos de la cuenta.", False
            Unload Me
            Exit Sub
        End If
'BUSCAMOS LOS PORCENTAJES DE COMISIONES DEL MESONERO Y LAS PROPIEDADES DEL CLIENTE: LIMITES DE CREDITOS, DIAS DE VENCIMIENTO,
        Call ClaseMeseros.PorcentajesDeComisionMesero(ClaseMeseros.VendedorCod)
        Call ClaseCliente.PropiedadesCliente(ClaseMeseros.ClienteCod, ConexionAdm)
        
        NombreCliente = ClaseCliente.ClienteDescripcion
        Identificacion = ClaseCliente.ClienteRif
        Telefono = ClaseCliente.ClienteTelefono
        
'SE BUSCAN LOS MONTO DE VENTAS PARA COMER AQUI = 0
        Call ClasePagos.TotalesConsumo(Organizacion, Localidad, Relacion, CuentaNumero, ServicioNumero, FSubtotalComision, FImpuestosComision, FDescuentoComision, 0)           'AQUI
        FTotalComision = FSubtotalComision + FImpuestosComision - FDescuentoComision
        
'SE BUSCAN LOS MONTO DE VENTAS PARA LLEVAR = 1
        Call ClasePagos.TotalesConsumo(Organizacion, Localidad, Relacion, CuentaNumero, ServicioNumero, FSubtotalSinComision, FImpuestosSinComision, FDescuentoSinComision, 1)  'LLEVAR
        FTotalSinComision = FSubtotalSinComision + FImpuestosSinComision - FDescuentoSinComision
        
'CALCULAMOS EL MONTO DE LA COMISION
        MontoComision = FormatNumber(CalcularMontoXComision(FSubtotalComision, FDescuentoComision, FImpuestosComision, FTotalComision, ClaseMeseros.PorcentajesComision), RsMonedaFacturar!n_decimales)

'BUSCAMOS LOS SUBTOTALES DE LA CUENTA EN EL MAESTRO DE CONSUMO
        FSubtotal = FormatNumber(ClaseDenominaciones.CambioDenominacion(RsCuenta!ns_subtotal, 1, RsMonedaFacturar!N_FACTOR, RsMonedaFacturar!n_decimales), RsMonedaFacturar!n_decimales)
        FDescuento = FormatNumber(ClaseDenominaciones.CambioDenominacion(FDescuento, 1, RsMonedaFacturar!N_FACTOR, RsMonedaFacturar!n_decimales), RsMonedaFacturar!n_decimales)
        FImpuestos = FormatNumber(ClaseDenominaciones.CambioDenominacion(RsCuenta!ns_impuestos, 1, RsMonedaFacturar!N_FACTOR, RsMonedaFacturar!n_decimales), RsMonedaFacturar!n_decimales)

'SUMAMOS ESOS MONTOS + LA COMISION
        FTotal = FormatNumber(FSubtotal - FDescuento + FImpuestos + MontoComision, RsMonedaFacturar!n_decimales)
        
        Me.subtotal = FormatNumber(FSubtotal, RsMonedaFacturar!n_decimales)
        Me.Descuento = FormatNumber(FDescuento, RsMonedaFacturar!n_decimales)
        Me.impuesto = FormatNumber(FImpuestos, RsMonedaFacturar!n_decimales)
        Me.servicio = FormatNumber(MontoComision, RsMonedaFacturar!n_decimales)

'VERIFICAMOS AL CLIENTE PARA VER CUANTO LE QUEDA DE CR�DITO
        MontoCredito = ClaseCliente.ClienteLimite - ClaseCliente.ClienteMontoCredito
        MontoCredito = FormatNumber(ClaseDenominaciones.CambioDenominacion(MontoCredito, 1, RsMonedaFacturar!N_FACTOR, RsMonedaFacturar!n_decimales), RsMonedaFacturar!n_decimales)
        
        Me.totalB = FormatNumber(FTotal, RsMonedaFacturar!n_decimales)
        'SI EL MONTO A PAGAR ES 0 NO GRABA
        MontoVuelto = 0
        If FTotal <= 0 Then
           Rutinas.Mensajes "El cliente no tiene consumos realizados.", False
           Unload Me
        End If
        'CONSTRUIR EL GRID DE LA FORMA DE PAGO.
        ClasePagos.ConstruirGrid Me.FormasPagos, RsMonedaFacturar!c_descripcion, RsMonedaFacturar!C_CODMONEDA, RsMonedaFacturar!n_decimales, RsMonedaFacturar!N_FACTOR, RsMonedaFacturar!N_FACTOR
        
        'CONFIGURA LOS FRAMES
        Call ConfigFrames
        'CALCULA LOS MONTOS A PARTIR DE LAS ENTRADAS DE DENOMINACION
        Call CalcularNuevoMonto
        
        'COLOCA EL MONTO DE LA FACTURA POR DEFECTO
        FormasPagos.S_FijarValor 1, 9, totalB.Caption
        FormaCargada = True
        
    End If
    RegionCritical = False
    Exit Sub
Falla_Forma:
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim PosAct As Variant
    
    If RegionCritical Then Exit Sub
    If FrmPagos.Datos.Visible And FrmPagos.Datos.Enabled Then
        Exit Sub
    End If
    
    Select Case Shift
        Case vbAltMask
            Select Case KeyCode
                Case vbKeyF6        'FORMA
                    
                    RegionCritical = True
                    PosAct = FormasPagos.sp_CeldaActual
                    FrmImprimirForma.FCuentaNo = FormasPagos.S_TomarValor(CInt(PosAct(0)), 8)
                    FrmImprimirForma.FDecimales = 2
                    FrmImprimirForma.FEmisor = FormasPagos.S_TomarValor(CInt(PosAct(0)), 6)
                    FrmImprimirForma.FMonto = FormasPagos.S_TomarValor(CInt(PosAct(0)), 9)
                    FrmImprimirForma.ClaseBancos.InicializarConexiones ConexionAdm, ConexionPos
                    Set FrmImprimirForma.ConexionPosLocal = Me.ConexionPosLocal
                    Set FrmImprimirForma.ConexionAdmLocal = Me.ConexionAdmLocal
                    
                    If Trim(FrmImprimirForma.FEmisor) = "" Then
                        GRutinas.Mensajes "No se ha especificado la entidad emisora de la denominaci�n"
                        RegionCritical = False
                        Exit Sub
                    End If
                    
                    If Trim(FrmImprimirForma.FMonto) = "" Or Not IsNumeric(FrmImprimirForma.FMonto) Then
                        GRutinas.Mensajes "Error en el monto de la denominaci�n"
                        RegionCritical = False
                        Exit Sub
                    ElseIf CDbl(FrmImprimirForma.FMonto) <= 0 Then
                        GRutinas.Mensajes "Monto inferior o igual a 0"
                        RegionCritical = False
                        Exit Sub
                    End If
                    
                    FrmImprimirForma.Show vbModal
                    SaltarImprimir = FrmImprimirForma.IgnorarProximo
                    RegionCritical = False
                    Set FrmImprimirForma = Nothing
                    Exit Sub
                    
                Case vbKeyE
                    Rutinas.SendKeys Chr(vbKeyF7), True
                Case vbKeyG
                    Rutinas.SendKeys Chr(vbKeyF4), True
                Case vbKeyB
                    Rutinas.SendKeys Chr(vbKeyF2), True
                Case vbKeyF
                    Rutinas.SendKeys "{%}" & Chr(vbKeyF6), True
                Case vbKeyC
                    Call Form_KeyDown(vbKeyF6, 0)
'                    Rutinas.SendKeys Chr(vbKeyF6), True
                Case vbKeyS
                    Rutinas.SendKeys Chr(vbKeyF12), True
                Case vbKeyA
                    Rutinas.SendKeys Chr(vbKeyF1), True
            End Select
            
        Case vbCtrlMask
            
            Select Case KeyCode
                Case vbKeyF6        'ENDOSO
                
            End Select
        
        Case Else
            
            Select Case KeyCode
                Case vbKeyF1        'AYUDA
                Case vbKeyF2        'BUSCAR
                    CuentasAUnir = ClaseMeseros.ShowCuentasDelCliente(CodigoCliente, Relacion)
                    If Not IsEmpty(CuentasAUnir) Then
                        If UnirLasCuentas(CuentasAUnir) Then
                            ResetearSiSalir = True      'ESTABLECE SI TRUE SI SE UNIERON CUENTA Y FUNCIONA PARA RESETEAR EL GRID DESPUES DE UNIR LA CUENTA.
                            FormaCargada = False
                            Call Form_Activate
                        Else
                            ResetearSiSalir = False
                        End If
                    End If
                Case vbKeyF4        'GRABAR
                    RegionCritical = True
                    Call GrabarDatos
                    RegionCritical = False
                Case vbKeyF7        'ELIMINAR
                    FormasPagos.SetFocus
                    Rutinas.SendKeys Chr(vbKeyDelete), True
                Case vbKeyF6        'CONFORMAR
                    
                    RegionCritical = True
                    PosAct = FormasPagos.sp_CeldaActual
                    FrmConformar.FIdentificacionCliente = Identificacion
                    FrmConformar.FNombreCliente = NombreCliente
                    FrmConformar.FTelefonoCliente = Telefono
                    FrmConformar.FOperadorConformacion = FormasPagos.S_TomarValor(CInt(PosAct(0)), 16)
                    FrmConformar.FClaveConformacion = FormasPagos.S_TomarValor(CInt(PosAct(0)), 17)
                    FrmConformar.FCuentaNo = FormasPagos.S_TomarValor(CInt(PosAct(0)), 8)
                    FrmConformar.FDecimales = 2
                    FrmConformar.FEmisor = FormasPagos.S_TomarValor(CInt(PosAct(0)), 6)
                    FrmConformar.FMonto = FormasPagos.S_TomarValor(CInt(PosAct(0)), 9)
                    FrmConformar.ClaseBancos.InicializarConexiones ConexionAdm, ConexionPos
                    
                    Set FrmConformar.ConexionPosLocal = Me.ConexionPosLocal
                    Set FrmConformar.ConexionAdmLocal = Me.ConexionAdmLocal
                    
                    If Trim(FrmConformar.FCuentaNo) = "" Then
                        GRutinas.Mensajes "No se ha especificado el n�mero de la denominaci�n"
                        RegionCritical = False
                        Exit Sub
                    End If
                    
                    If Trim(FrmConformar.FEmisor) = "" Then
                        GRutinas.Mensajes "No se ha especificado la entidad emisora de la denominaci�n"
                        RegionCritical = False
                        Exit Sub
                    End If
                    
                    FrmConformar.Show vbModal
                    Identificacion = FrmConformar.FIdentificacionCliente
                    NombreCliente = FrmConformar.FNombreCliente
                    Telefono = FrmConformar.FTelefonoCliente
                    
                    SaltarConformar = FrmConformar.IgnorarProximo
                    If FrmConformar.FOperadorConformacion <> "" Or FrmConformar.FClaveConformacion <> "" Then
                        'ESCRIBIRLO EN LA DENOMINACION
                        Call ClasePagos.EscribirConformacion(FormasPagos, CInt(PosAct(0)), FrmConformar.FOperadorConformacion, FrmConformar.FClaveConformacion)
                    End If
                    Set FrmConformar = Nothing
                    RegionCritical = False
                    Exit Sub
                
                Case vbKeyF12
                    Unload Me
                    GrabarOk = ResetearSiSalir
            End Select
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Dim PosAct As Variant
    PosAct = FormasPagos.sp_CeldaActual
    If Trim(FormasPagos.S_TomarValor(CInt(PosAct(0)), 4)) = Trim(ModPosWise.DesCash) Then
        Call FormasPagos.sm_activarKeypressCol(8, False)
    Else
        Call FormasPagos.sm_activarKeypressCol(8, True)
    End If
End Sub

Private Sub Form_Load()
    RegionCritical = True
End Sub

Private Sub FormasPagos_DblClick()
    Dim PosAct As Variant
    PosAct = FormasPagos.sp_CeldaActual
    UltimaTecla = vbKeyReturn
    Call ClasePagos.ProcesarTeclas(FormasPagos, Datos, RsMonedaFacturar!N_FACTOR, CInt(PosAct(1)), MontoVuelto, Me.Left + FormasPagos.CellLeft + FormasPagos.Left, Me.Top + FormasPagos.CellTop + FormasPagos.Top, vbKeyReturn)
    If FormasPagos.Enabled Then FormasPagos.SetFocus
End Sub

Private Sub FormasPagos_EnterCell()
    If FormasPagos.S_TomarValor(FormasPagos.Rows - 1, 4) = "" Then
        Rutinas.SendKeys Chr(vbKeyEscape), True
    End If
End Sub

Private Sub FormasPagos_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim PosAct As Variant, Conforma As Boolean, ImprimeForma As Boolean, Endosar As Boolean
    PosAct = FormasPagos.sp_CeldaActual
    UltimaTecla = 0
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeyUp
                    Call ClasePagos.LeerConfiguracionDenominacion(FormasPagos, CInt(PosAct(0)), ImprimeForma, Conforma, Endosar)
                    Toolbar1.Buttons("forma").Enabled = ImprimeForma
                    Toolbar1.Buttons("conformar").Enabled = Conforma
                    
                Case vbKeyDown
                    Call ClasePagos.LeerConfiguracionDenominacion(FormasPagos, CInt(PosAct(0)), ImprimeForma, Conforma, Endosar)
                    Toolbar1.Buttons("forma").Enabled = ImprimeForma
                    Toolbar1.Buttons("conformar").Enabled = Conforma
                    
                Case vbKeyDelete
                    Call CalcularNuevoMonto
                    Call ClasePagos.LeerConfiguracionDenominacion(FormasPagos, CInt(PosAct(0)), ImprimeForma, Conforma, Endosar)
                    Toolbar1.Buttons("forma").Enabled = ImprimeForma
                    Toolbar1.Buttons("conformar").Enabled = Conforma
                    
                Case vbKeyReturn
                    If PosAct(0) < FormasPagos.Rows - 1 Then Rutinas.SendKeys Chr(vbKeyEscape), True
                    If PosAct(1) = 9 Then
                        Call ClasePagos.ProcesarTeclas(FormasPagos, Datos, RsMonedaFacturar!N_FACTOR, CInt(PosAct(1)), MontoVuelto, Me.Left + FormasPagos.CellLeft + FormasPagos.Left, Me.Top + FormasPagos.CellTop + FormasPagos.Top, UltimaTecla)
                    End If
                    
            End Select
    End Select
    DoEvents
    If FormasPagos.Enabled Then
        FormasPagos.SetFocus
    End If
End Sub

Private Sub FormasPagos_LeaveCell()
    Dim PosAct As Variant
    PosAct = FormasPagos.sp_CeldaActual
    If Trim(TxtMontoProcesar.Caption) = "" Then
        Exit Sub
    End If
    Call ClasePagos.SalidaDeCelda(Me.FormasPagos, RsMonedaFacturar!c_descripcion, RsMonedaFacturar!C_CODMONEDA, RsMonedaFacturar!N_FACTOR, RsMonedaFacturar!N_FACTOR, CDbl(TxtMontoProcesar.Caption), MontoVuelto, UltimaTecla)
    Call CalcularNuevoMonto
    Call ActivarBotones
End Sub

Private Sub CalcularNuevoMonto()
    
    Me.Cancel.Caption = FormatNumber(Me.FormasPagos.sm_TotalizarColumna(10), RsMonedaFacturar!n_decimales)
    DoEvents
    If Trim(TxtMontoProcesar.Caption) = "" Then
        Exit Sub
    End If
    If Trim(Me.Cancel.Caption) = "" Then
        Exit Sub
    End If
    Me.vuelto.Caption = FormatNumber(CDbl(TxtMontoProcesar.Caption) - CDbl(Me.Cancel.Caption), RsMonedaFacturar!n_decimales)
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    If FrmPagos.Datos.Visible And FrmPagos.Datos.Enabled Then
        Exit Sub
    End If

    Select Case UCase(Button.Key)
        Case "ELIMINAR"
            Rutinas.SendKeys Chr(vbKeyF7)
        Case "GRABAR"
            Rutinas.SendKeys Chr(vbKeyF4)
        Case "BUSCAR"
            Rutinas.SendKeys Chr(vbKeyF2)
        Case "AYUDA"
            Rutinas.SendKeys Chr(vbKeyF1)
        Case "FORMA"
            Rutinas.SendKeys "{%}" & Chr(vbKeyF6)
        Case "ENDOSO"
            Rutinas.SendKeys "{^}" & Chr(vbKeyF6)
        Case "CONFORMAR"
            Rutinas.SendKeys Chr(vbKeyF6)
        Case "SALIR"
            Rutinas.SendKeys Chr(vbKeyF12)
    End Select
    
End Sub

Private Sub ActivarBotones()
    If FormasPagos.sm_BuscarValor(13, "1", S_mbNormal) > 0 Then
        Toolbar1.Buttons("forma").Enabled = True
    Else
        Toolbar1.Buttons("forma").Enabled = False
    End If
    If FormasPagos.sm_BuscarValor(14, "1", S_mbNormal) > 0 Then
        Toolbar1.Buttons("conformar").Enabled = True
    Else
        Toolbar1.Buttons("conformar").Enabled = False
    End If
End Sub


Private Function CalcularMontoXComision(ByRef LSubTotal As Double, ByRef LDescuento As Double, ByRef LImpuesto As Double, ByRef LTotal, ArregloImpuestos As Variant) As Double
    Dim LCont As Integer, ComisionImpuesto As Double
    On Error GoTo FALLA_LOCAL
    ComisionImpuesto = 0
    
    If ArregloImpuestos(LCont, 1) = 1 Then          'se cobra al subtotal
        If ArregloImpuestos(LCont, 2) Then          'SE CONSIDERA PARA IMPUESTO
            'NO SE CONOCE LA SOLUCION PARA ESTE CASO
            'SE POSTERGA LA SOLUCION
        Else                                        'NO SE CONSIDERA
            ComisionImpuesto = ComisionImpuesto + (LSubTotal * (ArregloImpuestos(LCont, 0) / 100))
            LTotal = LSubTotal - LDescuento + LImpuesto + ComisionImpuesto
        End If
    ElseIf ArregloImpuestos(LCont, 1) = 2 Then      'se cobra al total
        ComisionImpuesto = ComisionImpuesto + (LTotal * (ArregloImpuestos(LCont, 0) / 100))
        LTotal = LTotal + ComisionImpuesto
    End If
    CalcularMontoXComision = ComisionImpuesto
    Exit Function
FALLA_LOCAL:
End Function

Private Sub ConfigFrames()
    If Trim(Me.servicio.Caption) = "" Then Me.servicio.Caption = 0
    If CDbl(Me.servicio.Caption) = 0 Then
        Me.servicio.Visible = False
        Me.Lblcargo.Visible = False
    End If
    If Trim(Me.Descuento.Caption) = "" Then Me.Descuento.Caption = 0
    If CDbl(Me.Descuento.Caption) = 0 Then
        Me.Descuento.Visible = False
        Me.LblDescuento.Visible = False
    End If
End Sub

Private Sub GrabarDatos()
    Dim LCont As Integer, ImprimeForma As Boolean, Conforma As Boolean, Endosar As Boolean, BotonPulsado As Boolean
    'VERIFICAMOS CADA RENGLON DEL GRID HASTA LA ULTIMA LINEA, O DEBE QUEDAR UNA LINEA SIN DETALLE.
    On Error GoTo FALLA_LOCAL
    If Datos.Visible Then
        Exit Sub
    End If
    For LCont = 1 To FormasPagos.Rows - 1
        
        'EXISTE UN RENGLON DE "DENOMINACION" QUE NO TIENE COMPLETA SU INFORMACION
        If Trim(FormasPagos.S_TomarValor(LCont, 9)) = "" Then
            Unload Me
            Exit Sub
        End If
        If (FormasPagos.S_TomarValor(LCont, 5) <> "" And UCase(Trim(FormasPagos.S_TomarValor(LCont, 5))) <> "EFECTIVO") And CDbl(FormasPagos.S_TomarValor(LCont, 9)) <= 0 Then
            Rutinas.Mensajes "Existe un rengl�n que no se complet� su informaci�n, elim�nelo o completelo.", False
            Exit Sub
        End If
        
        'EXISTE UN RENGLON DE "EFECTIVO" QUE NO TIENE COMPLETA SU INFORMACION
        If Trim(UCase(FormasPagos.S_TomarValor(LCont, 4))) <> "EFECTIVO" And (Trim(FormasPagos.S_TomarValor(LCont, 8)) = "" Or Trim(FormasPagos.S_TomarValor(LCont, 7)) = "") Then
            Rutinas.Mensajes "Existe un rengl�n que no se complet� su informaci�n, elim�nelo o completelo.", False
            Exit Sub
        End If
        
        'LEE LA CONFIGURACION DE LA DENOMINACION PARA REVISAR LA CONFORMACI�N, LA CLAVE Y EL OPERADOR
        Call ClasePagos.LeerConfiguracionDenominacion(FormasPagos, LCont, ImprimeForma, Conforma, Endosar)
        
        'LAS VARIABLES: IMPRIMEFORMA = ES BOOLEANA Y DICE SI LA FORMA DEBE IMPRIMIRSE Y _
        SALTARIMPRIMIR = ES BOOLEANA E INDICA SI DEBE SALTAR �STA _
        EL PROBLEMA DE ESTA ULTIMA ES QUE NO SE SI AL AUTORIZAR QUE NO IMPRIMA ESTA SE ACTIVA.
        
        If ImprimeForma And Not SaltarImprimir Then
            Call Form_KeyDown(vbKeyF6, vbAltMask)
        End If
        
        'IDEM ANTERIOR, PERO (BUT) CON CONFORMACION
        If Conforma And Not SaltarConformar Then
            Call Form_KeyDown(vbKeyF6, 0)
        End If
    
    Next LCont
    'CLASE DE GRABAR LA FACTURA CAPA # 01
    If Trim(Me.vuelto.Caption) = "" Then
        Unload Me
        Exit Sub
    End If
    If Trim(Me.Cancel.Caption) = "" Then
        Unload Me
        Exit Sub
    End If
    If ClasePagos.GrabarFactura(Organizacion, Localidad, ServicioNumero, CuentaNumero, Caja, Turno, Usuario, Mesero, CodigoCliente, MonedaFacturar, RsMonedaFacturar!N_FACTOR, FSubtotal, FImpuestos, FDescuento, MontoComision, FTotal, CDbl(Me.Cancel.Caption), CDbl(Me.vuelto.Caption), MontoCredito, FormasPagos, "VEN", , isPosFood) Then
        GrabarOk = True
        Rutinas.DormirSistema POS.ESPERA_TOTALIZAR
        Unload Me
    Else
        Unload Me
        Exit Sub
    End If
    Exit Sub
FALLA_LOCAL:
    Call GRutinas.GrabarErrores(CStr(Err.Number), Err.Description, POS.UsuarioDelPos.Codigo, Err.Source, "FrmPagos", Err.Source, ConexionPos)
    Unload Me
End Sub

Private Function UnirLasCuentas(ACuentasUnir As Variant) As Boolean
    Dim LCont As Integer
    UnirLasCuentas = False
    For LCont = LBound(ACuentasUnir) To UBound(ACuentasUnir)
        Call ClaseMesas.UnirCuentas(ConexionPos, CStr(ACuentasUnir(LCont, 0)), CuentaNumero)
        UnirLasCuentas = True
    Next LCont
End Function
