VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmBancos 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   2955
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   3435
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2955
   ScaleWidth      =   3435
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox TxtBusqueda 
      Height          =   450
      Left            =   1050
      TabIndex        =   1
      Top             =   2490
      Width           =   2355
   End
   Begin MSComctlLib.ListView BANCOS 
      Height          =   2490
      Left            =   -15
      TabIndex        =   0
      Top             =   -15
      Width           =   3435
      _ExtentX        =   6059
      _ExtentY        =   4392
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      OLEDragMode     =   1
      OLEDropMode     =   1
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
      NumItems        =   0
   End
   Begin VB.Image Bteclado 
      BorderStyle     =   1  'Fixed Single
      Height          =   450
      Left            =   0
      Picture         =   "FrmBancos.frx":0000
      Stretch         =   -1  'True
      Top             =   2490
      Width           =   1050
   End
End
Attribute VB_Name = "FrmBancos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ClaseBancos As New Cls_Bancos
Public Rutinas As New cls_Rutinas
Public ConexionAdm As Object
Public ConexionPos As Object
Public Codigo As String
Private RsBancos As Object
Private Busqueda As String

Private Sub BANCOS_DblClick()
    ClaseBancos.CodBanco = FrmBancos.BANCOS.SelectedItem.Text
    ClaseBancos.DesBanco = FrmBancos.BANCOS.SelectedItem.ListSubItems(1).Text
    Me.Visible = False
End Sub

Private Sub BANCOS_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And Shift = 0 Then
        ClaseBancos.CodBanco = FrmBancos.BANCOS.SelectedItem.Text
        ClaseBancos.DesBanco = FrmBancos.BANCOS.SelectedItem.ListSubItems(1).Text
        Me.Visible = False
    End If
End Sub

Private Sub Bteclado_Click()
    TxtBusqueda.SetFocus
    Rutinas.LlamarTeclado TxtBusqueda
End Sub

'Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'    If Shift = 0 Then
'        Select Case KeyCode
'            Case vbKeyBack
'                If Len(Busqueda) > 0 Then Busqueda = Mid(Busqueda, 1, Len(Busqueda) - 1)
'                Call BuscarEnLista(BANCOS, Busqueda, True)
'            Case 32 To 255
'                Busqueda = Busqueda & Chr(KeyCode)
'                Call BuscarEnLista(BANCOS, Busqueda, True)
'        End Select
'    End If
'End Sub
'
Private Sub Form_Load()
    ClaseBancos.InicializarConexiones ConexionAdm, ConexionPos
    ClaseBancos.BuscarBanco RsBancos, Codigo
    Busqueda = ""
    Call ConfigReportView(RsBancos)
    TxtBusqueda.Width = BANCOS.Width - TxtBusqueda.Left - 100
End Sub

Private Sub ConfigReportView(Registros As Object)
    Dim ActItem As ListItem
    
    BANCOS.Width = 0
    BANCOS.Left = 0

    BANCOS.ColumnHeaders.Add 1, , "CODIGO", 0
    BANCOS.ColumnHeaders.Add 2, , "DESCRIPCION", BANCOS.Width - (BANCOS.Left)
    While Not RsBancos.EOF
        Set ActItem = BANCOS.ListItems.Add(, , RsBancos!C_CODIGO)
        ActItem.SubItems(1) = RsBancos!c_DESCRIPCIO
        
        Ancho = Rutinas.CalcularAncho(Registros!c_DESCRIPCIO, Me)
        If CLng(BANCOS.Width) < CLng(Ancho) Then
            BANCOS.Width = Ancho
            BANCOS.ColumnHeaders(2).Width = Ancho
            Me.Width = Ancho
        End If
        
        RsBancos.MoveNext
    Wend
End Sub

Public Sub BuscarEnLista(Listado As ListView, Palabra As String, Optional DarFoco As Boolean = True)
    Dim LCont As Long, DondeEsta As Long
    For LCont = 1 To Listado.ListItems.Count
         DondeEsta = InStr(1, UCase(Mid(Listado.ListItems(LCont).ListSubItems(1).Text, 1, Len(Palabra))), UCase(Palabra))
         If DondeEsta > 0 Then
            If DarFoco Then
                Listado.SetFocus
            End If
            Listado.ListItems(LCont).Selected = True
            Listado.ListItems(LCont).EnsureVisible
            Exit Sub
         End If
    Next LCont
End Sub

Private Sub TxtBusqueda_Change()
    Call BuscarEnLista(BANCOS, TxtBusqueda.Text, False)
End Sub

Private Sub TxtBusqueda_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyReturn Then
        Call BuscarEnLista(BANCOS, TxtBusqueda.Text)
    End If
End Sub
