VERSION 5.00
Begin VB.Form FrmImprimirForma 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Impresi�n de Forma"
   ClientHeight    =   3510
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4575
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3510
   ScaleWidth      =   4575
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cancelar 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      Height          =   975
      Left            =   3315
      Picture         =   "FrmImprimirForma.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2430
      Width           =   1170
   End
   Begin VB.CommandButton aceptar 
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      Height          =   975
      Left            =   2070
      Picture         =   "FrmImprimirForma.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2430
      Width           =   1170
   End
   Begin VB.CheckBox Chk_NoEndosable 
      Alignment       =   1  'Right Justify
      Caption         =   "Marcar No Endosable"
      CausesValidation=   0   'False
      Height          =   390
      Left            =   60
      TabIndex        =   0
      Top             =   1365
      Width           =   4440
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Entidad Emisora"
      Height          =   240
      Left            =   60
      TabIndex        =   9
      Top             =   135
      Width           =   1560
   End
   Begin VB.Label LblEmisor 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "##############"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   240
      Left            =   1875
      TabIndex        =   8
      Top             =   150
      Width           =   2670
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Cuenta Numero"
      Height          =   240
      Left            =   60
      TabIndex        =   7
      Top             =   510
      Width           =   1515
   End
   Begin VB.Label LblCuenta 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "##############"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   240
      Left            =   1875
      TabIndex        =   6
      Top             =   525
      Width           =   2670
   End
   Begin VB.Label LblMonto 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   240
      Left            =   1875
      TabIndex        =   5
      Top             =   915
      Width           =   2670
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Verifique el monto"
      Height          =   240
      Left            =   60
      TabIndex        =   4
      Top             =   900
      Width           =   1785
   End
   Begin VB.Label LblMensaje 
      BackStyle       =   0  'Transparent
      Caption         =   "Prepare la impresora e inserte la forma para escribirla."
      Height          =   555
      Left            =   60
      TabIndex        =   1
      Top             =   1845
      Width           =   4440
   End
End
Attribute VB_Name = "FrmImprimirForma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public FMonto As Double, FDecimales As Long, FCuentaNo As String, FEmisor As String
Public ClaseBancos As New Cls_Bancos
Public ConexionPosLocal As Object
Public ConexionAdmLocal As Object

Private ClaseRutinas As New cls_Rutinas
Private ClaseDispositivos As New clsDispositivos
Private ClaseReportes As New Cls_Reportes
Private NumeroDispositivo As Long
Private EsteDispositivo As Dispositivos
Private DatosEmpresa As Variant
Public IgnorarProximo As Boolean
Private BotonCancelar As Variant

Private Sub aceptar_Click()
    On Error GoTo FALLA_LOCAL
    EsteDispositivo = POS.Propiedades.MisDispositivos(NumeroDispositivo)
    If Not EsteDispositivo.OComApuntador Is Nothing Then
        'IMPRIMIR POR PUERTOS SERIALES
        Call ClaseBancos.Imp_ChequeSerial(EsteDispositivo.OComApuntador, CStr(DatosEmpresa(0)), CStr(DatosEmpresa(2)), FMonto, CStr(Date), Chk_NoEndosable.Value = 1)
    ElseIf Not EsteDispositivo.OOPOSApuntador Is Nothing Then
        'IMPRIMIR POR OPOS
        MsgBox "Falla en el OPOS DE IMPRESI�N"
    Else
        'IMPRIMIR POR LPT
        'Call ClaseBancos.Imp_Cheque(CStr(DatosEmpresa(0)), CStr(DatosEmpresa(2)), FMonto, Date, EsteDispositivo.Puerto, Chk_NoEndosable.Value = 1)
        If Trim(POS.ImpresoraCheque) = "" Then
            Call ClaseReportes.Imp_ChequeRtf(CStr(DatosEmpresa(0)), CStr(DatosEmpresa(2)), FMonto, Date, , Chk_NoEndosable.Value = 1)
        Else
            Call ClaseReportes.Imp_ChequeRtf(CStr(DatosEmpresa(0)), CStr(DatosEmpresa(2)), FMonto, Date, POS.ImpresoraCheque, Chk_NoEndosable.Value = 1, POS.TipoPuertoImpresoraCheque)
        End If
    End If
    Unload Me
    Exit Sub
FALLA_LOCAL:
    ClaseRutinas.Mensajes Err.Description, False
    Unload Me
End Sub

Private Sub cancelar_Click()
    If PosClass.FunctionTeclasAutorizar(-3, 0, POS.POSNo, CInt(POS.UsuarioDelPos.Nivel), POS.Branch, ConexionAdmLocal, ConexionPosLocal, POS.Forma) Then
        If ClaseRutinas.Mensajes("�Desea ignorar las pr�ximas solicitudes?", True) Then
            IgnorarProximo = True
        End If
        Unload Me
    End If
End Sub

Private Sub Form_Activate()
    If ConexionPosLocal Is Nothing Then
        GRutinas.Mensajes "No se pudo hacer referencia a una conexi�n...!", False
        End
    End If
    If FMonto <= 0 Then
        ClaseRutinas.Mensajes "No se especific� un monto para esta denominaci�n.", False
        Unload Me
    End If
    BotonCancelar = PosClass.NivelDeTecla(-3, 0, POS.POSNo, POS.Branch, ConexionPosLocal)
    If Not IsEmpty(BotonCancelar) Then
        If UBound(BotonCancelar) >= 10 Then
            Me.cancelar.Enabled = BotonCancelar(5)
        Else
            Me.cancelar.Enabled = False
        End If
    Else
        Me.cancelar.Enabled = False
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = 0 And (KeyCode = vbKeyEscape Or KeyCode = vbKeyF12) Then
        Call cancelar_Click
    End If
End Sub

Private Sub Form_Load()
    Me.LblMonto.Caption = FormatNumber(FMonto, FDecimales)
    Me.LblCuenta.Caption = FCuentaNo
    Me.LblEmisor.Caption = FEmisor
    NumeroDispositivo = ClaseDispositivos.SelectDispositivo("IMPRESORA")
    DatosEmpresa = ClaseBancos.DatosEmpresa
End Sub
