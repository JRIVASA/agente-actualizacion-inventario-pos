VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form FrmDenominaciones 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   2190
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   3165
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2190
   ScaleWidth      =   3165
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox TxtBusqueda 
      Height          =   465
      Left            =   1080
      TabIndex        =   1
      Top             =   1725
      Width           =   2040
   End
   Begin MSComctlLib.ListView DENOMINACIONES 
      Height          =   1710
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3150
      _ExtentX        =   5556
      _ExtentY        =   3016
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      OLEDragMode     =   1
      OLEDropMode     =   1
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
      NumItems        =   0
   End
   Begin VB.Image Bteclado 
      BorderStyle     =   1  'Fixed Single
      Height          =   450
      Left            =   15
      Picture         =   "FrmDenominaciones.frx":0000
      Stretch         =   -1  'True
      Top             =   1725
      Width           =   1050
   End
End
Attribute VB_Name = "FrmDenominaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Moneda As String
Public ClaseDenominaciones As New cls_Denominaciones
Public ConexionAdm As Object
Public ConexionPos As Object
Public RsDenominaciones As Object
Public Rutinas As New cls_Rutinas
Private Const MinLen As Long = 3000

Private Sub Bteclado_Click()
    TxtBusqueda.SetFocus
    Rutinas.LlamarTeclado TxtBusqueda
End Sub

Private Sub DENOMINACIONES_DblClick()
    ClaseDenominaciones.CodMoneda = Moneda
    ClaseDenominaciones.CodDenomina = FrmDenominaciones.Denominaciones.SelectedItem.Text
    ClaseDenominaciones.DesDenomina = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(1).Text
    ClaseDenominaciones.ValorDenomina = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(2).Text
    ClaseDenominaciones.esReal = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(3).Text
    ClaseDenominaciones.UsaPos = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(4).Text
    ClaseDenominaciones.MontoDeCompra = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(5).Text
    ClaseDenominaciones.MontoDevuelto = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(6).Text
    ClaseDenominaciones.PermiteDarVuelto = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(7).Text
    ClaseDenominaciones.RequiereDeEndoso = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(8).Text
    ClaseDenominaciones.RequiereDeConformacion = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(9).Text
    ClaseDenominaciones.ImprimeLaForma = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(10).Text
    Me.Visible = False
End Sub

Private Sub DENOMINACIONES_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And Shift = 0 Then
        ClaseDenominaciones.CodMoneda = Moneda
        ClaseDenominaciones.CodDenomina = FrmDenominaciones.Denominaciones.SelectedItem.Text
        ClaseDenominaciones.DesDenomina = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(1).Text
        ClaseDenominaciones.ValorDenomina = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(2).Text
        ClaseDenominaciones.esReal = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(3).Text
        ClaseDenominaciones.UsaPos = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(4).Text
        ClaseDenominaciones.MontoDeCompra = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(5).Text
        ClaseDenominaciones.MontoDevuelto = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(6).Text
        ClaseDenominaciones.PermiteDarVuelto = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(7).Text
        ClaseDenominaciones.RequiereDeEndoso = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(8).Text
        ClaseDenominaciones.RequiereDeConformacion = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(9).Text
        ClaseDenominaciones.ImprimeLaForma = FrmDenominaciones.Denominaciones.SelectedItem.ListSubItems(10).Text
        Me.Visible = False
    End If
End Sub

Private Sub Form_Activate()
    If RsDenominaciones.EOF Then Unload Me
End Sub

Private Sub Form_Load()
    ClaseDenominaciones.InicializarConexiones ConexionAdm, ConexionPos
    ClaseDenominaciones.BuscarDenominacion RsDenominaciones, Moneda, , 0
    Call ConfigReportView(RsDenominaciones)
End Sub

Private Sub ConfigReportView(Registros As Object)
    Dim ActItem As ListItem
    
    Denominaciones.Width = 0
    Denominaciones.Left = 0
    
    Denominaciones.ColumnHeaders.Add 1, , "CODIGO", 0
    Denominaciones.ColumnHeaders.Add 2, , "DESCRIPCION", Denominaciones.Width
    Denominaciones.ColumnHeaders.Add 3, , "VALOR", 0
    Denominaciones.ColumnHeaders.Add 4, , "REAL", 0
    Denominaciones.ColumnHeaders.Add 5, , "USAPOS", 0
    Denominaciones.ColumnHeaders.Add 6, , "MONTOCOMPRA", 0
    Denominaciones.ColumnHeaders.Add 7, , "MONTOVUELTO", 0
    Denominaciones.ColumnHeaders.Add 8, , "PERMITEVUELTO", 0
    Denominaciones.ColumnHeaders.Add 9, , "REQUIEREENDOSO", 0
    Denominaciones.ColumnHeaders.Add 10, , "IMPRIMEFORMA", 0
    Denominaciones.ColumnHeaders.Add 11, , "REQUIERECONFORMACION", 0
    If Not Registros.EOF Then
        While Not Registros.EOF
            Set ActItem = Denominaciones.ListItems.Add(, , Registros!C_CODdenomina)
            ActItem.SubItems(1) = Registros!c_denominacion
            ActItem.SubItems(2) = Registros!n_valor
            ActItem.SubItems(3) = Registros!c_real
            ActItem.SubItems(4) = Registros!c_pos
            ActItem.SubItems(5) = Registros!n_monto_compra
            ActItem.SubItems(6) = Registros!n_monto_vuelto
            ActItem.SubItems(7) = Registros!b_permite_vuelto
             ActItem.SubItems(8) = Registros!nu_requiere_endoso
            ActItem.SubItems(9) = Registros!nu_imprime_forma
            ActItem.SubItems(10) = Registros!nu_requiere_conformacion
            
            Ancho = Rutinas.CalcularAncho(Registros!c_denominacion, Me)
            If CLng(Denominaciones.Width) < CLng(Ancho) Then
                Denominaciones.Width = Ancho
                Denominaciones.ColumnHeaders(2).Width = Ancho
                Me.Width = Ancho
            End If
            If Denominaciones.Width < MinLen Then
                Denominaciones.Width = MinLen
                Denominaciones.ColumnHeaders(2).Width = MinLen
                Me.Width = MinLen
            End If
            
            Registros.MoveNext
        Wend
        Registros.MoveFirst
    End If
End Sub


Public Sub BuscarEnLista(Listado As ListView, Palabra As String, Optional DarFoco As Boolean = True)
    Dim LCont As Long, DondeEsta As Long
    For LCont = 1 To Listado.ListItems.Count
         DondeEsta = InStr(1, UCase(Mid(Listado.ListItems(LCont).ListSubItems(1).Text, 1, Len(Palabra))), UCase(Palabra))
         If DondeEsta > 0 Then
            If DarFoco Then
                Listado.SetFocus
            End If
            Listado.ListItems(LCont).Selected = True
            Listado.ListItems(LCont).EnsureVisible
            Exit Sub
         End If
    Next LCont
End Sub

Private Sub TxtBusqueda_Change()
    Call BuscarEnLista(Denominaciones, TxtBusqueda.Text, False)
End Sub

Private Sub TxtBusqueda_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyReturn Then
        Call BuscarEnLista(Denominaciones, TxtBusqueda.Text)
    End If
End Sub
