VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Denominaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'//**************************************************************************//
'// REPUBLICA BOLIVARIANA DE VENEZUELA                          20/08/2003
'// STELLAR POS FOOD REV. 1.0.0
'// SUNLIT CONSULTORES S.A.
'//
'// CLASE DE ENTIDADES BANCARIAS
'// CAPAS: 02 Y 03
'//
'//**************************************************************************//
Private Const CdNvo As String = "NULL**"
Private Const ClienteContado As String = "9999999999"
Private Rutinas As New cls_Rutinas

Private ConexionAdm                     As Object
Private ConexionPos                     As Object

'PROPIEDADES DE LA ENOMINACION
Private CodigoMoneda                    As String
Private DescripcionDenominacion         As String
Private CodigoDenominacion              As String
Private Valor                           As Double
Private real                            As Boolean
Private POS                             As Boolean
Private MontoCompra                     As Double
Private MontoVuelto                     As Double
Private PermiteVuelto                   As Boolean
Private RequiereEndoso                  As Boolean
Private ImprimeForma                    As Boolean
Private RequiereConformacion            As Boolean
Private RequiereSerial                  As Boolean

Property Let CodMoneda(Codigo As String)
    CodigoMoneda = Codigo
End Property

Property Let DesDenomina(Descripcion As String)
    DescripcionDenominacion = Descripcion
End Property

Property Let CodDenomina(CodDen As String)
    CodigoDenominacion = CodDen
End Property

Property Let ValorDenomina(ValorDen As Integer)
    Valor = ValorDen
End Property

Property Let esReal(RealDen As Boolean)
    real = RealDen
End Property

Property Let UsaPos(UsaElPos As Boolean)
    POS = UsaElPos
End Property

Property Let PermiteDarVuelto(DaVuelto As Boolean)
    PermiteVuelto = DaVuelto
End Property

Property Let MontoDeCompra(MontoCompras As Double)
    MontoCompra = MontoCompras
End Property

Property Let MontoDevuelto(MontoVueltos As Double)
    MontoVuelto = MontoVueltos
End Property

Property Let RequiereDeEndoso(Endosa As Boolean)
    RequiereEndoso = Endosa
End Property

Property Let ImprimeLaForma(ImprimirForma As Boolean)
    ImprimeForma = ImprimirForma
End Property

Property Let RequiereDeConformacion(Conforma As Boolean)
    RequiereConformacion = Conforma
End Property

Property Let RequiereDeSeriales(Seriales As Boolean)
    RequiereSerial = Conforma
End Property

'GETS
Property Get CodMoneda() As String
    CodMoneda = CodigoMoneda
End Property

Property Get DesDenomina() As String
    DesDenomina = DescripcionDenominacion
End Property

Property Get CodDenomina() As String
    CodDenomina = CodigoDenominacion
End Property

Property Get ValorDenomina() As Integer
    ValorDenomina = Valor
End Property

Property Get esReal() As Boolean
    esReal = real
End Property

Property Get UsaPos() As Boolean
    UsaPos = POS
End Property

Property Get PermiteDarVuelto() As Boolean
    PermiteDarVuelto = PermiteVuelto
End Property

Property Get MontoDeCompra() As Double
    MontoDeCompra = MontoCompra
End Property

Property Get MontoDevuelto() As Double
    MontoDevuelto = MontoVuelto
End Property

Property Get RequiereDeEndoso() As Boolean
    RequiereDeEndoso = RequiereEndoso
End Property

Property Get ImprimeLaForma() As Boolean
    ImprimeLaForma = ImprimeForma
End Property

Property Get RequiereDeConformacion() As Boolean
    RequiereDeConformacion = RequiereConformacion
End Property

Property Get RequiereDeSeriales() As Boolean
    RequiereDeSeriales = RequiereSerial
End Property

'FIN PROPIEDADES
Public Sub InicializarConexiones(pConexionAdm As Object, pConexionPos As Object, Optional sConexionAdm As String, Optional sConexionPos As String)
    Call Rutinas.AbrirConexion(pConexionAdm, sConexionAdm)         'CREAR CONEXION
    Call Rutinas.AbrirConexion(pConexionPos, sConexionPos)          'CREAR CONEXION
    Set ConexionAdm = pConexionAdm
    Set ConexionPos = pConexionPos
End Sub

Public Function CambioDenominacion(Valor As Double, Factor As Double, FactorDestino As Double, decimales As Integer, Optional CadaValor As Boolean = False) As Double
    CambioDenominacion = -1
    If FactorDestino > 0 Then
        If CadaValor Then
            CambioDenominacion = FormatNumber(FormatNumber(Valor, decimales) * (FormatNumber(Factor, decimales) / FormatNumber(FactorDestino, decimales)), decimales)
        Else
            CambioDenominacion = FormatNumber((Valor * Factor) / FactorDestino, decimales)
        End If
    End If
End Function

Public Function BuscarDenominacion(Optional ByRef Registros As Object, Optional Moneda As String = "", Optional Codigo As String = "", Optional VReal As Integer = 2, Optional DelPos As Boolean = True, Optional OrderDesc As Boolean = False) As Boolean
    '0 = valor real
    '1 = sin valor real
    '2 = todas
    'CAPA # 02
    BuscarDenominacion = False
    If IsMissing(Registros) Then Exit Function
    
    If Registros Is Nothing Then
        Set Registros = Rutinas.CrearAdoRs()
    End If
    
    BuscarDenominacion = Buscar_Denominacion(Registros, Moneda, Codigo, VReal, DelPos, OrderDesc)
    
End Function

Private Function Buscar_Denominacion(ByRef Registros As Object, Moneda As String, Codigo As String, VReal As Integer, Optional DelPos As Boolean, Optional OrderDesc As Boolean, Optional Tabla As String = "MA_DENOMINACIONES") As Boolean
    'CAPA # 03
    Dim LRec As Object, ClausulaWhere As String
    On Error GoTo Falla_Local
    Buscar_Denominacion = False
    Set LRec = Rutinas.CrearAdoRs()
    ClausulaWhere = ""
    
    If Codigo <> "" Then
        ClausulaWhere = ClausulaWhere & " C_CODDENOMINA = '" & Codigo & "'"
    End If
    
    If ClausulaWhere <> "" And VReal < 2 Then
        ClausulaWhere = ClausulaWhere & " AND C_REAL = " & IIf(VReal = 1, "1", "0")
    ElseIf VReal < 2 And ClausulaWhere = "" Then
        ClausulaWhere = ClausulaWhere & " C_REAL = " & IIf(VReal = 1, "1", "0")
    End If
    
    If ClausulaWhere <> "" Then
        ClausulaWhere = ClausulaWhere & " AND C_CODMONEDA = '" & Moneda & "'"
    Else
        ClausulaWhere = ClausulaWhere & " C_CODMONEDA = '" & Moneda & "'"
    End If
    
    If DelPos Then
        If ClausulaWhere <> "" Then
            ClausulaWhere = ClausulaWhere & " AND c_pos = 1"
        Else
            ClausulaWhere = ClausulaWhere & " c_pos = 1"
        End If
    End If
    
    If ClausulaWhere <> "" Then
        ClausulaWhere = " WHERE " & ClausulaWhere
    End If
    If OrderDesc Then
        LRec.Open "select * FROM " & Tabla & ClausulaWhere & " ORDER BY N_VALOR, C_DENOMINACION", ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    Else
        LRec.Open "select * FROM " & Tabla & ClausulaWhere & " ORDER BY N_VALOR DESC, C_DENOMINACION", ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    End If
    Rutinas.CopiarRegistro LRec, Registros
    If Not LRec.EOF Then
        CodigoMoneda = LRec!C_CODMONEDA
        DescripcionDenominacion = LRec!c_denominacion
        CodigoDenominacion = LRec!C_CODdenomina
        Valor = LRec!n_valor
        real = LRec!c_real
        POS = LRec!c_pos
        MontoCompra = LRec!n_monto_compra
        MontoVuelto = LRec!n_monto_vuelto
        PermiteVuelto = LRec!b_permite_vuelto
        RequiereEndoso = LRec!nu_requiere_endoso
        RequiereConformacion = LRec!nu_imprime_forma
        ImprimeForma = LRec!nu_requiere_conformacion
        RequiereSerial = LRec!nu_requiere_serial
        Buscar_Denominacion = True
    End If
    
    LRec.Close
    Exit Function
Falla_Local:
    Exit Function
End Function

Public Sub ShowFrmDenominaciones(X As Integer, Y As Integer, Moneda As String)
    Set FrmDenominaciones.ConexionAdm = ConexionAdm
    Set FrmDenominaciones.ConexionPos = ConexionPos
    FrmDenominaciones.Moneda = Moneda
    FrmDenominaciones.Left = X
    FrmDenominaciones.Top = Y
    FrmDenominaciones.Show vbModal
    
    Set RsDenominaciones = FrmDenominaciones.RsDenominaciones
    CodigoMoneda = FrmDenominaciones.ClaseDenominaciones.CodMoneda
    DescripcionDenominacion = FrmDenominaciones.ClaseDenominaciones.DesDenomina
    CodigoDenominacion = FrmDenominaciones.ClaseDenominaciones.CodDenomina
    Valor = FrmDenominaciones.ClaseDenominaciones.ValorDenomina
    real = FrmDenominaciones.ClaseDenominaciones.esReal
    POS = FrmDenominaciones.ClaseDenominaciones.UsaPos
    MontoCompra = FrmDenominaciones.ClaseDenominaciones.MontoDeCompra
    MontoVuelto = FrmDenominaciones.ClaseDenominaciones.MontoDevuelto
    PermiteVuelto = FrmDenominaciones.ClaseDenominaciones.PermiteDarVuelto
    RequiereEndoso = FrmDenominaciones.ClaseDenominaciones.RequiereDeEndoso
    RequiereConformacion = FrmDenominaciones.ClaseDenominaciones.RequiereDeConformacion
    ImprimeForma = FrmDenominaciones.ClaseDenominaciones.ImprimeLaForma
    RequiereSerial = FrmDenominaciones.ClaseDenominaciones.RequiereDeSeriales
    
    Set FrmDenominaciones = Nothing
End Sub


