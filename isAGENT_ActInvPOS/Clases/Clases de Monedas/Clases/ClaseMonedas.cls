VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Monedas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'//**************************************************************************//
'// REPUBLICA BOLIVARIANA DE VENEZUELA                          20/08/2003
'// STELLAR POS FOOD REV. 1.0.0
'// SUNLIT CONSULTORES S.A.
'//
'// CLASE DE ENTIDADES BANCARIAS
'// CAPAS: 02 Y 03
'//
'//**************************************************************************//
Private Const CdNvo As String = "NULL**"
Private Const ClienteContado As String = "9999999999"
Private Rutinas As New cls_Rutinas

Private ConexionAdm                     As Object
Private ConexionPos                     As Object

'PROPIEDADES DE LA MONEDA
Private CodigoMoneda                    As String
Private DescripcionMoneda               As String
Private Factor                          As Double
Private PreferenciaMoneda               As Boolean
Private ActivaMoneda                    As Boolean
Private SimboloMoneda                   As String
Private DecimalesMoneda                 As Integer

Property Get Denominaciones() As Object
    Set Denominaciones = RsDenominaciones
End Property

Property Let CodMoneda(Codigo As String)
    CodigoMoneda = Codigo
End Property

Property Let DesMoneda(Descripcion As String)
    DescripcionMoneda = Descripcion
End Property

Property Let FacMoneda(MonFactor As Double)
    Factor = MonFactor
End Property

Property Let PrefMoneda(Preferencia As Boolean)
    PreferenciaMoneda = Preferencia
End Property

Property Let ActMoneda(activa As Boolean)
    ActivaMoneda = activa
End Property

Property Let SimMoneda(Simbolo As String)
    SimboloMoneda = Simbolo
End Property

Property Let DecMoneda(MonDec As Integer)
    DecimalesMoneda = MonDec
End Property

'GETS
Property Get CodMoneda() As String
    CodMoneda = CodigoMoneda
End Property

Property Get DesMoneda() As String
    DesMoneda = DescripcionMoneda
End Property

Property Get FacMoneda() As Double
    FacMoneda = Factor
End Property

Property Get PrefMoneda() As Boolean
    PrefMoneda = PreferenciaMoneda
End Property

Property Get ActMoneda() As Boolean
    ActMoneda = ActivaMoneda
End Property

Property Get SimMoneda() As String
    SimMoneda = SimboloMoneda
End Property

Property Get DecMoneda() As Integer
    DecMoneda = DecimalesMoneda
End Property

'FIN PROPIEDADES

Public Sub InicializarConexiones(pConexionAdm As Object, pConexionPos As Object, Optional sConexionAdm As String, Optional sConexionPos As String)
    Call Rutinas.AbrirConexion(pConexionAdm, sConexionAdm)         'CREAR CONEXION
    Call Rutinas.AbrirConexion(pConexionPos, sConexionPos)          'CREAR CONEXION
    Set ConexionAdm = pConexionAdm
    Set ConexionPos = pConexionPos
End Sub

Public Function BuscarMonedas(Optional ByRef Registros As Object, Optional Codigo As String = "", Optional Preferencia As Integer = 2, Optional activa As Boolean = True) As Boolean
    
    '0 = las no prefrenciales
    '1 = las preferenciales
    '2 = todas
    'CAPA # 02
    BuscarMonedas = False
    If IsMissing(Registros) Then Exit Function
    
    If Registros Is Nothing Then
        Set Registros = Rutinas.CrearAdoRs()
    End If
    
    BuscarMonedas = Buscar_Monedas(Registros, Codigo, Preferencia, activa)
    
End Function

Private Function Buscar_Monedas(ByRef Registros As Object, Codigo As String, Preferencia As Integer, activa As Boolean, Optional Tabla As String = "MA_MONEDAS") As Boolean
    'CAPA # 03
    Dim LRec As Object, ClausulaWhere As String
    On Error GoTo Falla_Local
    Buscar_Monedas = False
    Set LRec = Rutinas.CrearAdoRs()
    ClausulaWhere = ""
    
    If Codigo <> "" Then
        ClausulaWhere = ClausulaWhere & " C_CODMONEDA = '" & Codigo & "'"
    End If
    
    If ClausulaWhere <> "" And Preferencia < 2 Then
        ClausulaWhere = ClausulaWhere & " AND B_PREFERENCIA = " & IIf(Preferencia = 1, "1", "0")
    ElseIf Preferencia < 2 And ClausulaWhere = "" Then
        ClausulaWhere = ClausulaWhere & " B_PREFERENCIA = " & IIf(Preferencia = 1, "1", "0")
    ElseIf Preferencia = 2 Then
        ClausulaWhere = ClausulaWhere & " B_PREFERENCIA = 1"
    End If
    
    If ClausulaWhere <> "" Then
        ClausulaWhere = ClausulaWhere & " AND B_ACTIVA = " & IIf(activa, "1", "0")
    Else
        ClausulaWhere = ClausulaWhere & " B_ACTIVA = " & IIf(activa, "1", "0")
    End If
    
    If ClausulaWhere <> "" Then
        ClausulaWhere = " WHERE " & ClausulaWhere
    End If
    
    LRec.Open "select * FROM " & Tabla & ClausulaWhere, ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    Rutinas.CopiarRegistro LRec, Registros
    If Preferencia = 1 Then
        If Not LRec.EOF Then
            
            CodigoMoneda = LRec!C_CODMONEDA
            DescripcionMoneda = LRec!c_descripcioN
            Factor = LRec!N_FACTOR
            PreferenciaMoneda = LRec!b_preferencia
            ActivaMoneda = LRec!b_activa
            SimboloMoneda = LRec!c_simbolo
            DecimalesMoneda = LRec!n_decimales
            Buscar_Monedas = True

        End If
    Else
        
        If Not LRec.EOF Then
            
            CodigoMoneda = LRec!C_CODMONEDA
            DescripcionMoneda = LRec!c_descripcioN
            Factor = LRec!N_FACTOR
            PreferenciaMoneda = LRec!b_preferencia
            ActivaMoneda = LRec!b_activa
            SimboloMoneda = LRec!c_simbolo
            DecimalesMoneda = LRec!n_decimales
            Buscar_Monedas = True

        End If
        
    End If
    LRec.Close
    Exit Function
Falla_Local:
    Exit Function
End Function

Public Function TieneDenominaciones(CodigoMoneda As String, Optional CnLocal As Object, Optional DelPos As Boolean = True) As Boolean
    Dim RsDenominaciones As Object
    Set RsDenominaciones = Rutinas.CrearAdoRs
    If Trim(CodigoMoneda) = "" Then Exit Function
    If IsMissing(ConexionAdm) Then
        If IsMissing(CnLocal) Then Exit Function
        Set ConexionAdm = CnLocal
    End If
    TieneDenominaciones = Tiene_Denominaciones(CodigoMoneda, DelPos)
End Function

Private Function Tiene_Denominaciones(CodigoMoneda As String, DelPos As Boolean, Optional Tablas As String = "MA_DENOMINACIONES") As Boolean
    Dim RsDenominaciones As Object
    Set RsDenominaciones = Rutinas.CrearAdoRs
    RsDenominaciones.Open "SELECT * FROM " & Tablas & " WHERE c_codmoneda = '" & CodigoMoneda & "' " & IIf(DelPos, " AND C_POS = 1", ""), ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    Tiene_Denominaciones = Not RsDenominaciones.EOF
End Function

Public Sub ShowFrmMonedas(X As Integer, Y As Integer)
    Set FrmMonedas.ConexionAdm = ConexionAdm
    Set FrmMonedas.ConexionPos = ConexionPos
    FrmMonedas.Left = X
    FrmMonedas.Top = Y
    FrmMonedas.Show vbModal
    ActMoneda = FrmMonedas.Clasemonedas.ActMoneda
    CodMoneda = FrmMonedas.Clasemonedas.CodMoneda
    DecMoneda = FrmMonedas.Clasemonedas.DecMoneda
    DesMoneda = FrmMonedas.Clasemonedas.DesMoneda
    FacMoneda = FrmMonedas.Clasemonedas.FacMoneda
    PrefMoneda = FrmMonedas.Clasemonedas.PrefMoneda
    SimMoneda = FrmMonedas.Clasemonedas.SimMoneda
    Set FrmMonedas = Nothing
End Sub

Public Function FormatoMoneda(LConexion As Object, Valor As Double, Optional decimales As Integer = 0, Optional CodigoMoneda As String = "") As Double
    Dim RecMoneda As Object
    If Trim(CodigoMoneda) = "" Then
        FormatoMoneda = Rutinas.FormatearValor(Valor, decimales, FormatDouble)
    Else
        If Not LConexion Is Nothing Then
            Set RecMoneda = Rutinas.CrearAdoRs
            RecMoneda.Open "SELECT * FROM MA_MONEDAS WHERE C_CODMONEDA = '" & CodigoMoneda & "'", LConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
            If Not RecMoneda.EOF Then
                FormatoMoneda = Rutinas.FormatearValor(Valor, RecMoneda!n_decimales, FormatDouble)
            Else
                FormatoMoneda = Rutinas.FormatearValor(Valor, 0, FormatDouble)
            End If
            RecMoneda.Close
        End If
    End If
End Function
