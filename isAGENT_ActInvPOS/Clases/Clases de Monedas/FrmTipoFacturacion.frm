VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmTipoFacturacion 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5235
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   10050
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   349
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   670
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox TxtTotalAPagar 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      CausesValidation=   0   'False
      Height          =   360
      Left            =   1995
      TabIndex        =   11
      Top             =   4020
      Width           =   2895
   End
   Begin VB.TextBox TxtDescuento 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      CausesValidation=   0   'False
      Height          =   360
      Left            =   1995
      TabIndex        =   5
      Top             =   3120
      Width           =   2895
   End
   Begin VB.CommandButton cmd_numpad 
      Height          =   630
      Left            =   4275
      Picture         =   "FrmTipoFacturacion.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   4515
      Width           =   615
   End
   Begin VB.Frame FrameBotones 
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   30
      TabIndex        =   30
      Top             =   0
      Width           =   4905
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   495
         Left            =   75
         TabIndex        =   31
         Top             =   195
         Width           =   4740
         _ExtentX        =   8361
         _ExtentY        =   873
         ButtonWidth     =   847
         ButtonHeight    =   820
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         ImageList       =   "Icono_Apagado"
         HotImageList    =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   10
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "Eliminar"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Grabar"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "Consulta"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "forma"
               Object.ToolTipText     =   "Imprimir forma de fago"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "endoso"
               Object.ToolTipText     =   "Endoso de Denominaciones"
               ImageIndex      =   13
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "conformar"
               Object.ToolTipText     =   "Conformaci�n"
               ImageIndex      =   14
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Salir"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Ayuda"
               ImageIndex      =   10
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
   End
   Begin VB.Timer KeyBdStatus 
      Interval        =   150
      Left            =   135
      Top             =   4770
   End
   Begin VB.Frame FrmKeyPad 
      ClipControls    =   0   'False
      Enabled         =   0   'False
      Height          =   4455
      Left            =   5025
      TabIndex        =   16
      Top             =   765
      Width           =   3885
      Begin VB.CommandButton Cmd_ClearAll 
         Caption         =   "CLS"
         CausesValidation=   0   'False
         Height          =   780
         Left            =   2880
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   360
         Width           =   900
      End
      Begin VB.CommandButton cmd_Back 
         CausesValidation=   0   'False
         Height          =   780
         Left            =   1950
         Picture         =   "FrmTipoFacturacion.frx":0442
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   360
         Width           =   900
      End
      Begin VB.CommandButton Cmd_Insert 
         Caption         =   "Insert"
         CausesValidation=   0   'False
         Height          =   780
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   360
         Width           =   900
      End
      Begin VB.CommandButton Cmd_Decimal 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   1950
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   3600
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   9
         Left            =   1950
         Picture         =   "FrmTipoFacturacion.frx":074C
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   1170
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   8
         Left            =   1020
         Picture         =   "FrmTipoFacturacion.frx":1016
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   1170
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   7
         Left            =   90
         Picture         =   "FrmTipoFacturacion.frx":18E0
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   1170
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   6
         Left            =   1950
         Picture         =   "FrmTipoFacturacion.frx":21AA
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   1980
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   5
         Left            =   1020
         Picture         =   "FrmTipoFacturacion.frx":2A74
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   1980
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   4
         Left            =   90
         Picture         =   "FrmTipoFacturacion.frx":333E
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   1980
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   3
         Left            =   1950
         Picture         =   "FrmTipoFacturacion.frx":3C08
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   2790
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   2
         Left            =   1020
         Picture         =   "FrmTipoFacturacion.frx":44D2
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   2790
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   1
         Left            =   90
         Picture         =   "FrmTipoFacturacion.frx":4D9C
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   2790
         Width           =   900
      End
      Begin VB.CommandButton NUMEROS 
         CausesValidation=   0   'False
         Height          =   780
         Index           =   0
         Left            =   90
         Picture         =   "FrmTipoFacturacion.frx":5666
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   3600
         Width           =   1830
      End
      Begin VB.CommandButton Cmd_Enter 
         CausesValidation=   0   'False
         Height          =   3225
         Left            =   2880
         Picture         =   "FrmTipoFacturacion.frx":5F30
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   1155
         Width           =   900
      End
      Begin VB.CommandButton Cmd_NumLock 
         Caption         =   "Num Lock"
         CausesValidation=   0   'False
         Height          =   780
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   360
         Width           =   900
      End
      Begin VB.Shape InsertLed 
         BackColor       =   &H00008000&
         BorderColor     =   &H00008000&
         FillColor       =   &H00008000&
         FillStyle       =   0  'Solid
         Height          =   180
         Left            =   1365
         Shape           =   3  'Circle
         Top             =   135
         Visible         =   0   'False
         Width           =   150
      End
      Begin VB.Shape NumLockLed 
         BackColor       =   &H00008000&
         BorderColor     =   &H00008000&
         FillColor       =   &H00008000&
         FillStyle       =   0  'Solid
         Height          =   180
         Left            =   465
         Shape           =   3  'Circle
         Top             =   135
         Visible         =   0   'False
         Width           =   150
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   0
      Top             =   750
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":623A
            Key             =   "agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":6F16
            Key             =   "buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":7BF2
            Key             =   "modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":88CE
            Key             =   "cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":95AA
            Key             =   "borrar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":A286
            Key             =   "grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":AF62
            Key             =   "imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":BC3E
            Key             =   "graficar"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":C91A
            Key             =   "salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":CC36
            Key             =   "ayuda"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":D912
            Key             =   "TECLADO"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":19A4E
            Key             =   "IMPRIMIRFORMA"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":19EA0
            Key             =   "IMPRIMIRENDOSO"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1A2F2
            Key             =   "CONFORMAR"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   2220
      Top             =   990
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1A744
            Key             =   "agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1B420
            Key             =   "buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1B73C
            Key             =   "modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1BA58
            Key             =   "cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1C734
            Key             =   "borrar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1D410
            Key             =   "grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1E0EC
            Key             =   "imprimit"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1E408
            Key             =   "graficar"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1F0E4
            Key             =   "salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":1F400
            Key             =   "ayuda"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":200DC
            Key             =   "TECLADO"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":2C218
            Key             =   "IMPRIMIRFORMA"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":2C66A
            Key             =   "IMPRIMIRENDOSO"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTipoFacturacion.frx":2CABC
            Key             =   "CONFORMAR"
         EndProperty
      EndProperty
   End
   Begin VB.Label TxtRif 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   360
      Left            =   1995
      TabIndex        =   15
      Top             =   1785
      Width           =   2895
   End
   Begin VB.Label txtDireccion 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   360
      Left            =   1995
      TabIndex        =   14
      Top             =   1335
      Width           =   2895
   End
   Begin VB.Label TxtDescripcion 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   360
      Left            =   1995
      TabIndex        =   13
      Top             =   885
      Width           =   2895
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Total a Pagar"
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   4110
      Width           =   1335
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Neto a pagar"
      Height          =   240
      Left            =   135
      TabIndex        =   10
      Top             =   3660
      Width           =   1290
   End
   Begin VB.Label LblMontoNeto 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1995
      TabIndex        =   9
      Top             =   3570
      Width           =   2895
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Monto de factura"
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   2295
      Width           =   1725
   End
   Begin VB.Label LblMontoFactura 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1995
      TabIndex        =   7
      Top             =   2220
      Width           =   2895
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Descuento"
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   3195
      Width           =   1050
   End
   Begin VB.Label LblLimite 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1995
      TabIndex        =   4
      Top             =   2670
      Width           =   2895
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "L�mite de cr�dito"
      Height          =   240
      Left            =   135
      TabIndex        =   3
      Top             =   2745
      Width           =   1650
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Cliente"
      Height          =   240
      Left            =   135
      TabIndex        =   2
      Top             =   930
      Width           =   675
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Rif"
      Height          =   240
      Left            =   135
      TabIndex        =   1
      Top             =   1830
      Width           =   240
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Direcci�n"
      Height          =   240
      Left            =   135
      TabIndex        =   0
      Top             =   1380
      Width           =   900
   End
End
Attribute VB_Name = "FrmTipoFacturacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ObjTexto As Object
Dim ClaseClientes As New clsDatosCliente
Dim ClaseMeseros As New cls_mesoneros
Dim ClaseMonedas As New cls_Monedas
Dim ClaseDenominaciones As New cls_Denominaciones
Dim ClasePagos As New cls_Pagos
Dim Rutinas As New cls_Rutinas

Dim RsCuenta As Object
Dim RsMonedas As Object
Dim RsPreferencial As Object

Public ConexionAdm As Object
Public ConexionPos As Object

Public CodigoCliente As String
Public MonedaFacturar As String
Public CodigoCuenta As String
Public CodigoServicio As String
Public Relacion As String

Private LSubtotal As Double
Private LImpuestos As Double
Private LTotal As Double
Private LLimite As Double
Private MonedaCuenta As String
Private FactorCuenta As Double

Const FormaWidthAngosta As Long = 5040
Const FormaWidthAncha As Long = 9075
Const FrameWidthAngosta As Long = 327
Const FrameWidthAncha As Long = 596
Const ToolBar1WidthAngosta As Long = 4740
Const ToolBar1WidthAncha As Long = 8775

Private Sub Bteclado_Click()
    Call ClaseMesoneros.LlamarTeclado(ObjTexto)
End Sub

Private Sub cmd_Back_Click()
    Dim PosAct As Integer
    PosAct = ObjTexto.SelStart
    If Len(ObjTexto.Text) > 0 And PosAct > 0 Then
        ObjTexto.Text = Mid(ObjTexto.Text, 1, PosAct - 1) & Mid(ObjTexto.Text, PosAct + 1, Len(ObjTexto.Text))
        ObjTexto.SelStart = PosAct
    End If
    If ObjTexto.Enabled Then ObjTexto.SetFocus
End Sub

Private Sub Cmd_ClearAll_Click()
    ObjTexto.Text = ""
    If ObjTexto.Enabled Then ObjTexto.SetFocus
End Sub

Private Sub Cmd_Decimal_Click()
    Dim PosAct As Integer
    PosAct = ObjTexto.SelStart + 1
    If Rutinas.EstatusTecla(vbKeyInsert) Then
        ObjTexto.Text = Mid(ObjTexto.Text, 1, PosAct - 1) & Rutinas.SDecimal & Mid(ObjTexto.Text, PosAct, Len(ObjTexto.Text))
        ObjTexto.SelStart = PosAct
    Else
        ObjTexto.Text = Mid(ObjTexto.Text, 1, PosAct - 1) & Rutinas.SDecimal & Mid(ObjTexto.Text, PosAct + 1, Len(ObjTexto.Text))
        ObjTexto.SelStart = PosAct
    End If
    If ObjTexto.Enabled Then ObjTexto.SetFocus
End Sub

Private Sub Cmd_Enter_Click()
    If ObjTexto.Enabled Then ObjTexto.SetFocus
    Rutinas.SendKeys Chr(vbKeyReturn), True
End Sub

Private Sub Cmd_Insert_Click()
    Rutinas.SendKeys Chr(vbKeyInsert), True
    If ObjTexto.Enabled Then ObjTexto.SetFocus
End Sub

Private Sub Cmd_NumLock_Click()
    Rutinas.SendKeys Chr(vbKeyNumlock), True
    If ObjTexto.Enabled Then ObjTexto.SetFocus
End Sub

Private Sub cmd_numpad_Click()
    Call AjustarFormulario(True)
    ObjTexto.SetFocus
End Sub

Private Sub Form_Activate()

    If Trim(CodigoCliente) = "" Then Unload Me
    If Not ClaseClientes.PropiedadesCliente(CodigoCliente, ConexionAdm) Then Unload Me
    If Not ClaseMeseros.DatosDecuenta(CodigoCliente, CodigoCuenta, CodigoServicio, Relacion, RsCuenta) Then Unload Me
    If Not ClaseMonedas.BuscarMonedas(RsPreferencial, , 1, True) Then Unload Me
    If Not ClaseMonedas.BuscarMonedas(RsMonedas, MonedaFacturar, 2, True) Then Unload Me
    
    TxtDescripcion.Caption = ClaseMeseros.ClienteNom
    txtDireccion.Caption = ClaseClientes.ClienteDireccion
    TxtRif.Caption = ClaseClientes.ClienteRif
    LblLimite.Caption = FormatNumber(ClaseDenominaciones.CambioDenominacion(ClaseClientes.ClienteLimite - ClaseClientes.ClienteMontoCredito, RsPreferencial!n_factor, RsMonedas!n_factor, RsMonedas!N_DECIMALES), RsMonedas!N_DECIMALES)
    
    LLimite = CDbl(LblLimite.Caption)
    LSubtotal = ClaseDenominaciones.CambioDenominacion(ClaseMeseros.SubtotalCuenta, RsPreferencial!n_factor, RsMonedas!n_factor, RsMonedas!N_DECIMALES)
    LImpuestos = ClaseDenominaciones.CambioDenominacion(ClaseMeseros.ImpuestosCuenta, RsPreferencial!n_factor, RsMonedas!n_factor, RsMonedas!N_DECIMALES)
    LTotal = LSubtotal + LImpuestos
    
    TxtDescuento.Text = FormatNumber(0, RsMonedas!N_DECIMALES)
    TxtTotalAPagar.Text = FormatNumber(CDbl(LTotal) - CDbl(TxtDescuento.Text), RsMonedas!N_DECIMALES)
    LblMontoNeto.Caption = FormatNumber(LTotal, RsMonedas!N_DECIMALES)
    TxtDescuento.Enabled = (ClaseClientes.ClienteTipoDescuento = TipoFinanciero)
    
    If LTotal = 0 Then
        Call Rutinas.Mensajes("El cliente no tiene monto de consumo, si desea puede reintegrar la transacci�n actual, pulsando F6 y haciendo un reintegro total.", False)
        Unload Me
        Exit Sub
    End If
    
    Me.LblMontoFactura.Caption = FormatNumber(LTotal, RsMonedas!N_DECIMALES)
    Cmd_Decimal.Caption = Rutinas.SDecimal
    
    If CDbl(Me.LblLimite.Caption) = 0 Then
        MsgBox "Al Contado Solamente"
        Unload Me
        Exit Sub
    End If
    
    Call AjustarFormulario
    
End Sub

Private Sub Form_Load()
    ClaseMeseros.InicializarConexiones ConexionAdm, ConexionPos
    ClaseMonedas.InicializarConexiones ConexionAdm, ConexionPos
    ClaseDenominaciones.InicializarConexiones ConexionAdm, ConexionPos
End Sub

Private Sub KeyBdStatus_Timer()
    Dim Kbd As Object
    Set Kbd = CreateObject("DLLKeyboard.DLLTeclado")
    NumLockLed.Visible = Kbd.EstadoTeclas(vbKeyNumlock)
    InsertLed.Visible = Kbd.EstadoTeclas(vbKeyInsert)
End Sub

Private Sub NUMEROS_Click(Index As Integer)
    Dim PosAct As Integer
    PosAct = ObjTexto.SelStart + 1
    If Rutinas.EstatusTecla(vbKeyInsert) Then
        ObjTexto.Text = Mid(ObjTexto.Text, 1, PosAct - 1) & Chr(Index + 48) & Mid(ObjTexto.Text, PosAct, Len(ObjTexto.Text))
        ObjTexto.SelStart = PosAct
    Else
        ObjTexto.Text = Mid(ObjTexto.Text, 1, PosAct - 1) & Chr(Index + 48) & Mid(ObjTexto.Text, PosAct + 1, Len(ObjTexto.Text))
        ObjTexto.SelStart = PosAct
    End If
    If ObjTexto.Enabled Then ObjTexto.SetFocus
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case "GRABAR"
            If Not ValidarMontoAPagar Then Exit Sub
            
            If Trim(TxtTotalAPagar.Text) = "" Then Exit Sub
            If FormatNumber(CDbl(TxtTotalAPagar.Text), RsMonedas!N_DECIMALES) > 0 Then
                ClasePagos.InicializarConexiones ConexionAdm, ConexionPos
                'PARAMETROS DE ENTRADA: POSX,POSY, CODIGO DE LA MONEDA, EL SUBTOTAL DE LA FACTURA, EL DESCUENTO DE LA FACTURA, EL MONTO TOTAL DE IMPUESTOS, FALTA: EL NETO A COBRAR EN DENOMINACIONES
                ClasePagos.ShowFrmPagos 0, 0, RsMonedas!c_codmoneda, LSubtotal, CDbl(TxtDescuento.Text), LImpuestos, CDbl(Me.TxtTotalAPagar)
            End If
            'GRABAR DENOMINACIONES INTRODUCIDAS (TR_VENTAS_POS_PAGOS)
            'GRABAR FACTURA DE VENTAS Y DE IMPUESTOS DE FACTURA (MA_POS_VENTAS, MA_POS_VENTAS_IMPUESTOS)
            'GRABAR DETALLES DE FACTURA DE VENTA Y DE IMPUESTO DE DETALLES DE FACTURA (TR_POS_VENTAS, TR_POS_VENTAS_IMPUESTOS)
            'CONSOLIDAR LA CUENTA EN (MA_CONSUMO)
        Case "SALIR"
            Unload Me
            Set FrmTipoFacturacion = Nothing
    End Select
End Sub

Private Sub TxtDescuento_Change()
    Dim PosInvalid As Integer, PosAct As Integer
    PosAct = TxtDescuento.SelStart + 1
    If Not Rutinas.AutomataCheckCad(TxtDescuento.Text, Rutinas.SDecimal, Rutinas.SMil, PosInvalid) Then
        TxtDescuento.Text = Mid(TxtDescuento.Text, 1, PosInvalid - 1) & Mid(TxtDescuento.Text, PosInvalid + 1, Len(TxtDescuento.Text))
        TxtDescuento.SelStart = PosAct - 1
    End If
End Sub

Private Sub TxtDescuento_GotFocus()
    Set ObjTexto = TxtDescuento
End Sub

Private Sub TxtDescuento_KeyDown(KeyCode As Integer, Shift As Integer)
    If (KeyCode = vbKeyReturn Or KeyCode = vbKeyTab) And Shift = 0 Then
        If Trim(TxtDescuento.Text) = "" Then TxtDescuento.Text = "0"
        TxtDescuento.Text = Rutinas.FormatearValor(TxtDescuento.Text, RsMonedas!N_DECIMALES, FormatString)
        If CDbl(TxtDescuento.Text) > ((ClaseClientes.ClienteDescuento / 100) * LTotal) Then
            Rutinas.Mensajes "Al aplicar este monto de descuento se alcanzan cantidades negativas.", False
            TxtDescuento.Text = Rutinas.FormatearValor(((ClaseClientes.ClienteDescuento / 100) * LTotal), RsMonedas!N_DECIMALES, FormatString)
            If TxtDescuento.Enabled Then TxtDescuento.SetFocus
        Else
            LTotal = LSubtotal + LImpuestos - CDbl(TxtDescuento.Text)
            LblMontoNeto.Caption = Rutinas.FormatearValor(LTotal, RsMonedas!N_DECIMALES, FormatString)
            TxtTotalAPagar.Text = LblMontoNeto.Caption
            If TxtTotalAPagar.Enabled Then TxtTotalAPagar.SetFocus
        End If
    End If
End Sub

Private Sub TxtTotalAPagar_Change()
    Dim PosInvalid As Integer, PosAct As Integer
    PosAct = TxtTotalAPagar.SelStart + 1
    If Not Rutinas.AutomataCheckCad(TxtTotalAPagar.Text, Rutinas.SDecimal, Rutinas.SMil, PosInvalid) Then
        TxtTotalAPagar.Text = Mid(TxtTotalAPagar.Text, 1, PosInvalid - 1) & Mid(TxtTotalAPagar.Text, PosInvalid + 1, Len(TxtTotalAPagar.Text))
        TxtTotalAPagar.SelStart = PosAct - 1
    End If
End Sub

Private Sub TxtTotalAPagar_GotFocus()
    Set ObjTexto = TxtTotalAPagar
End Sub

Private Sub TxtTotalAPagar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And Shift = 0 Then
        Call ValidarMontoAPagar
    End If
End Sub

Private Function ValidarMontoAPagar() As Boolean
    ValidarMontoAPagar = False
    If Trim(TxtTotalAPagar.Text) = "" Then TxtTotalAPagar.Text = "0"
    TxtTotalAPagar.Text = Rutinas.FormatearValor(TxtTotalAPagar.Text, RsMonedas!N_DECIMALES, FormatString)
    If CDbl((TxtTotalAPagar.Text)) < LTotal - LLimite Then
        Call Rutinas.Mensajes("El monto a pagar debe ser superior o igual al monto limite de cr�dito", False)
        TxtTotalAPagar.Text = Rutinas.FormatearValor(LTotal - LLimite, RsMonedas!N_DECIMALES, FormatString)
        TxtTotalAPagar.SelStart = 0
        TxtTotalAPagar.SelLength = Len(TxtTotalAPagar)
        TxtTotalAPagar.SetFocus
        Exit Function
    ElseIf CDbl((TxtTotalAPagar.Text)) > LTotal Then
        Call Rutinas.Mensajes("El monto a pagar debe ser inferior o igual al monto de la factura", False)
        TxtTotalAPagar.Text = Rutinas.FormatearValor(LTotal, RsMonedas!N_DECIMALES, FormatString)
        TxtTotalAPagar.SelStart = 0
        TxtTotalAPagar.SelLength = Len(TxtTotalAPagar)
        TxtTotalAPagar.SetFocus
        Exit Function
    Else
        If TxtDescuento.Enabled Then TxtDescuento.SetFocus
    End If
    ValidarMontoAPagar = True
End Function

Private Sub AjustarFormulario(Optional SetFrmKeyPad As Boolean = False)
    If SetFrmKeyPad Then FrmKeyPad.Enabled = Not FrmKeyPad.Enabled
    If Not FrmKeyPad.Enabled Then
        Me.Width = FormaWidthAngosta
        Me.FrameBotones.Width = FrameWidthAngosta
        Me.Toolbar1.Width = ToolBar1WidthAngosta
        FrmKeyPad.Enabled = False
    Else
        Me.Width = FormaWidthAncha
        Me.FrameBotones.Width = FrameWidthAncha
        Me.Toolbar1.Width = ToolBar1WidthAncha
        FrmKeyPad.Enabled = True
    End If
    Me.Left = (Screen.Width \ 2) - (Me.Width \ 2)
End Sub
