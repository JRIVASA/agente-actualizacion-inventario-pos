VERSION 5.00
Begin VB.Form FrmTEST 
   Caption         =   "Form1"
   ClientHeight    =   5760
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5760
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command5 
      Caption         =   "CLASE DE DEVOLUCIONES"
      Height          =   450
      Left            =   45
      TabIndex        =   4
      Top             =   1995
      Width           =   4590
   End
   Begin VB.CommandButton Command4 
      Caption         =   "CLASE DE CLIENTES"
      Height          =   450
      Left            =   45
      TabIndex        =   3
      Top             =   1515
      Width           =   4590
   End
   Begin VB.CommandButton Command3 
      Caption         =   "CLASE DE SEPARAR MESAS"
      Height          =   450
      Left            =   45
      TabIndex        =   2
      Top             =   1020
      Width           =   4590
   End
   Begin VB.CommandButton Command2 
      Caption         =   "CLASE DE MONEDAS"
      Height          =   450
      Left            =   45
      TabIndex        =   1
      Top             =   540
      Width           =   4590
   End
   Begin VB.CommandButton Command1 
      Caption         =   "CLASE DE LOGIN MESONEROS"
      Height          =   450
      Left            =   45
      TabIndex        =   0
      Top             =   60
      Width           =   4590
   End
End
Attribute VB_Name = "FrmTEST"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim XMouse As Integer, YMouse As Integer
Dim Rutinas As New cls_Rutinas
Dim X As New clsCargarConfig
Dim Y As New clsDispositivos
Private Sub Command1_Click()
    Dim Meseros As New cls_mesoneros
    Meseros.InicializarConexiones Ent.BDD, Ent.POS
    Meseros.ShowFrmMesoneros "N000001", "UNI01", True
'    Meseros.ShowFrmMesoneros "N000003", "UNI01", False
    'N000003
'    Debug.Print Meseros.VendedorCod, Meseros.ClienteCod, Meseros.ClienteNom, Meseros.CuentaNum, Meseros.ServicioNum, Meseros.MesaNum
End Sub

Private Sub Command2_Click()
    Dim Cl As New cls_Pagos, Codigo As String, Descripcion As String, cl2 As New cls_Denominaciones
    Cl.InicializarConexiones Ent.BDD, Ent.POS, Ent.ADMLOCAL, Ent.POSLOCAL
'    Call Cl.ShowFrmTipoFacturacion(XMouse, YMouse, "0000000005", "0000000005", "9999999999", "N000001", "001", 1, "UNI01", "GLOBAL", "0000000011", 300, 10, 30)
    
    Call Cl.ShowFrmPagos("", "UNI01", "002", 8, "0000000011", "9999999999", "0000000009", "0000000009", "N000001", "0000000019", "9999999999")
'    cl2.InicializarConexiones Ent.BDD, Ent.POS
'    cl2.ShowFrmDenominaciones XMouse, YMouse, "0000000001"
End Sub

Private Sub Command3_Click()
    Dim Meseros As New cls_mesoneros
    Meseros.InicializarConexiones Ent.BDD, Ent.POS
    Meseros.ShowFrmSepararCuentas "N000001", "001", "0000000018"
    Debug.Print Meseros.VendedorCod, Meseros.ClienteCod, Meseros.ClienteNom, Meseros.CuentaNum, Meseros.ServicioNum, Meseros.MesaNum
End Sub

Private Sub Command4_Click()
    Dim Clientes As New clsDatosCliente
    Clientes.InicializarConexiones Ent.BDD, Ent.POS
    Clientes.ShowFrmClientes "0000000037", "N000001", "0000000018"
End Sub

Private Sub Command5_Click()
    FrmDevolucion.Show vbModal
End Sub

Private Sub Form_DblClick()
    MsgBox Rutinas.NombreDelComputador & " - " & CStr(Rutinas.SerialDelProcesador)
End Sub

Private Sub Form_Load()
    Dim FH As Long, w As New recsun.Obj_rtf
    Ent.BDD.Open
    Ent.POS.Open
    Ent.POSLOCAL.Open
    Ent.ADMLOCAL.Open
    POS.POSNo = "002"
    Call X.CargarDispositivosPos(Ent.POSLOCAL)
    Call Y.AbrirDispositivo(Y.SelectDispositivo(Y.Str_PNemotecnico(PNemotecnicoImpresora)), , , FH)
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    XMouse = X
    YMouse = Y
End Sub
