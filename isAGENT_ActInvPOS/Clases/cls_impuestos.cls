VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_impuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Dim cStruct As Struct

Private Type StructDatosImp
    BaseImpuesto        As Double
    MontoImpuesto       As Double
    PorcentajeImpuesto  As Double
End Type

Private Type Struct
    TablaMaPrincipal            As String ' Tabla que lleva el impuesto Ej: cxc, ma_ventas, ma_compras
    Conexion                    As New ADODB.Connection
    FactorPosrateoxDescuento    As Double ' para ver elfactor de posrateo para el impuesto, base, por motivo de un descuento Financiero
    TablaImpuestos              As String ' Tabla donde se va relacionar el valor de los impuestos con la tablama
    Documento                   As String
    TipoDoc                     As String
    Localidad                   As String
    CodProveedor                As String
    Datos()                     As StructDatosImp
End Type

Property Let Conexion(pConexion As ADODB.Connection)
    Set cStruct.Conexion = pConexion
End Property

Property Let FactorPosrateoxDescuento(pFactorPosrateoxDescuento As Double)
    cStruct.FactorPosrateoxDescuento = pFactorPosrateoxDescuento
End Property

Property Get FactorPosrateoxDescuento() As Double
    FactorPosrateoxDescuento = cStruct.FactorPosrateoxDescuento
End Property

Property Let Localidad(pLocalidad As String)
    cStruct.Localidad = pLocalidad
End Property

Property Get Localidad() As String
    Localidad = cStruct.Localidad
End Property

Property Let CodProveedor(pCodProveedor As String)
    cStruct.CodProveedor = pCodProveedor
End Property

Property Get CodProveedor() As String
    CodProveedor = cStruct.CodProveedor
End Property

Property Let Documento(pDocumento As String)
    cStruct.Documento = pDocumento
End Property

Property Get Documento() As String
    Documento = cStruct.Documento
End Property

Property Let TipoDoc(pTipoDoc As String)
    cStruct.TipoDoc = pTipoDoc
End Property

Property Get TipoDoc() As String
    TipoDoc = cStruct.TipoDoc
End Property

Property Let TablaMaPrincipal(pTablaMaPrincipal As String)
    cStruct.TablaMaPrincipal = pTablaMaPrincipal
End Property

Property Get TablaMaPrincipal() As String
    TablaMaPrincipal = cStruct.TablaMaPrincipal
End Property

Property Let TablaImpuestos(pTablaImpuestos As String)
    cStruct.TablaImpuestos = pTablaImpuestos
End Property

Property Get TablaImpuestos() As String
    TablaImpuestos = cStruct.TablaImpuestos
End Property

Public Function ImpuestoMaestras_Alicuotas() As Variant
    mLonArr = UBound(cStruct.Datos)
    If mLonArr Then
        ReDim mvalores(mLonArr)
        For i = 1 To mLonArr
            mvalores(i) = Array(cStruct.Datos(i).BaseImpuesto, cStruct.Datos(i).MontoImpuesto, cStruct.Datos(i).PorcentajeImpuesto)
        Next
        ImpuestoMaestras_Alicuotas = mvalores
    Else
        ImpuestoMaestras_Alicuotas = Empty
    End If
End Function

Public Sub ImpuestoMaestras_IniciliazarAlicuotas()
    ReDim cStruct.Datos(0)
End Sub

Public Sub ImpuestoMaestras_AcumularImpuesto(pBaseImpuesto As Double, pMontoImpuesto As Double, pProcentajeImpuesto As Double)
    Dim mLonArr As Integer
    Dim mAgregar As Boolean
    Dim mDatos() As StructDatosImp
    mLonArr = UBound(cStruct.Datos)
    mAgregar = True
    If pMontoImpuesto > 0 Then
        For i = 1 To mLonArr
            If cStruct.Datos(i).PorcentajeImpuesto = pProcentajeImpuesto Then
                cStruct.Datos(i).BaseImpuesto = cStruct.Datos(i).BaseImpuesto + pBaseImpuesto
                cStruct.Datos(i).MontoImpuesto = cStruct.Datos(i).MontoImpuesto + pMontoImpuesto
                mAgregar = False
                Exit For
            End If
        Next
        If mAgregar Then
            ImpuestoMaestras_Adicionar pBaseImpuesto, pMontoImpuesto, pProcentajeImpuesto
        End If
    End If
End Sub

Public Sub ImpuestoMaestras_Adicionar(pBaseImpuesto As Double, pMontoImpuesto As Double, pProcentajeImpuesto As Double)
    Dim mLonArr As Integer
    mLonArr = UBound(cStruct.Datos) + 1
    ReDim Preserve cStruct.Datos(mLonArr)
    cStruct.Datos(mLonArr).BaseImpuesto = pBaseImpuesto
    cStruct.Datos(mLonArr).MontoImpuesto = pMontoImpuesto
    cStruct.Datos(mLonArr).PorcentajeImpuesto = pProcentajeImpuesto
End Sub

Public Sub ImpuestoMaestras_Eliminar(pPos)
    Dim mLonArr As Integer
    Dim mDatos() As StructDatosImp
    mLonArr = UBound(cStruct.Datos) - 1
    ReDim mDatos(mLonArr)
    k = 0
    For i = 0 To mLonArr
        If i <> pPos Then
            mDatos(k) = cStruct.Datos(i)
        End If
        k = k + 1
    Next
End Sub

Public Function ImpuestoMaestras_Grabar() As Boolean
    Dim mLonArr As Integer
    Dim mRs As New ADODB.Recordset
    mLonArr = UBound(cStruct.Datos)
    ImpuestoMaestras_Grabar = False
    If mLonArr And cStruct.Documento <> "" And cStruct.TipoDoc <> "" And cStruct.TablaImpuestos <> "" Then
        mRs.Open "Select * from " & cStruct.TablaImpuestos & " Where 1=2", cStruct.Conexion, adOpenStatic, adLockBatchOptimistic
        For i = 1 To mLonArr
            mRs.AddNew
                mRs!cs_DOCUMENTO = cStruct.Documento
                mRs!cs_TIPODOC = cStruct.TipoDoc
                mRs!cs_codproveedor = cStruct.CodProveedor
                mRs!CS_CODLOCALIDAD = cStruct.Localidad
                mRs!ns_BaseImpuesto = cStruct.Datos(i).BaseImpuesto * (1 - cStruct.FactorPosrateoxDescuento)
                mRs!ns_MontoImpuesto = cStruct.Datos(i).MontoImpuesto * (1 - cStruct.FactorPosrateoxDescuento)
                mRs!ns_PorcentajeImpuesto = cStruct.Datos(i).PorcentajeImpuesto
            mRs.UpdateBatch
        Next
        ImpuestoMaestras_Grabar = True
    End If
End Function

Private Sub Class_Initialize()
    ReDim cStruct.Datos(0)
End Sub

Public Function ImpuestoMaestras_TotalMontoImpuesto()
    mLonArr = UBound(cStruct.Datos)
    mMontoImp = 0
    If mLonArr Then
        ReDim mvalores(mLonArr)
        For i = 1 To mLonArr
            mMontoImp = mMontoImp + cStruct.Datos(i).MontoImpuesto
        Next
    End If
    ImpuestoMaestras_TotalMontoImpuesto = mMontoImp
End Function

Public Function ImpuestoMaestras_InterfazImpuesto(pMonto As Double)
    ' Devuelve Monto Impuesto
    frm_Cls_impuesto.fMontoBase = pMonto
    Set frm_Cls_impuesto.fClsimpuesto = Me
    frm_Cls_impuesto.Show vbModal
End Function
