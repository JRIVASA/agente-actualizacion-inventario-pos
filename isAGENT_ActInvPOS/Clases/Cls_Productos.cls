VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cls_Productos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mRsProducto As Object
Private mConexion   As Object
Private mClsRutinas As New cls_Rutinas

Private Type sProducto
    cCodigo                     As String
    cDescri                     As String
    cDepartamento               As String
    cGrupo                      As String
    cSubgrupo                   As String
    cMarca                      As String
    cModelo                     As String
    cProcede                    As Boolean
    nCostoActual                As Double
    nCostoAnterior              As Double
    nCostoPromedio              As Double
    nCostoReposicion            As Double
    nPrecio1                    As Double
    nPrecio2                    As Double
    nPrecio3                    As Double
    cSeriales                   As String
    cCompuesto                  As String
    cPresenta                   As String
    nPeso                       As Double
    nVolumen                    As Double
    nCantiBul                   As Double
    nPesoBul                    As Double
    
End Type

Public Enum mtTipoProducto
    tpUnidad
    tpPesado
    tpPesable
    tpExtendidas
    tpInformativo
    tpCompuesto
    tpDesconocido = -1
End Enum

Property Get RecordSetProducto() As Object
    Set RecordSetProducto = mRsProducto
End Property

Property Let StrConexion(pStrConexion As String)
    Set mConexion = mClsRutinas.CrearAdoCn
    mConexion.conectionstring = pStrConexion
    mConexion.Open
End Property

Property Set ObjectConexion(ByVal pObjectConexion As Object)
    Set mConexion = pObjectConexion
End Property

Public Function BuscarProducto(pCodigo As String, ByRef pRs As Object) As Boolean
    Set mRsProducto = mClsRutinas.CrearAdoRs
    On Error GoTo Falla_Local
    BuscarProducto = False
    mRsProducto.Open "SELECT MA_PRODUCTOS.* FROM MA_PRODUCTOS LEFT JOIN MA_CODIGOS ON MA_CODIGOS.C_CODNASA = MA_PRODUCTOS.C_CODIGO WHERE MA_CODIGOS.C_CODIGO = '" & pCodigo & "'", mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
'    'DEBUG.PRINT "SELECT MA_PRODUCTOS.* FROM MA_PRODUCTOS LEFT JOIN MA_CODIGOS ON MA_CODIGOS.C_CODNASA = MA_PRODUCTOS.C_CODIGO WHERE MA_CODIGOS.C_CODIGO = '" & pCodigo & "'"
    Set pRs = mRsProducto
    BuscarProducto = Not mRsProducto.EOF
Falla_Local:
End Function

Public Function QueTipoProducto(ByVal pCodigoProducto As String, ByVal pConexion As Object) As mtTipoProducto
    
    Dim RsProd As New ADODB.Recordset
    
    On Error GoTo Falla_Local
    
    QueTipoProducto = tpDesconocido
    Set RsProd = mClsRutinas.CrearAdoRs
    
    'octavio 10/09/2004
    mClsRutinas.Apertura_Recordset RsProd, adUseServer
    'mClsRutinas.Apertura_Recordset RsProd, adUseClient
    
    RsProd.Open _
    "SELECT N_TIPOPESO " & _
    "FROM MA_PRODUCTOS " & _
    "WHERE C_CODIGO = '" & pCodigoProducto & "' ", _
    pConexion, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If Not RsProd.EOF Then
        Select Case RsProd!N_TIPOPESO
            Case 0
                QueTipoProducto = tpUnidad
            Case 1
                QueTipoProducto = tpPesado
            Case 2
                QueTipoProducto = tpPesable
            Case 3
                QueTipoProducto = tpExtendidas
            Case 4
                QueTipoProducto = tpInformativo
            Case 5
                QueTipoProducto = tpCompuesto
        End Select
    End If
    
    RsProd.Close
    
Falla_Local:

End Function

Public Function EscribirComponentesProductos(ByVal pLocalidad As String, ByVal pDocumento As String, _
ByVal pCodigoProducto As String, ByVal pDescripcion As String, pFechaDocumento As Date, _
ByVal pCantidadFactor As Double, pLinea As Long, ByVal pTipoMovimiento As String, _
ByVal pDeposito As String, ByVal pCosto As Double, ByVal pPrecio As Double, _
ByVal pSubtotal As Double, ByVal pDescuento As Double, ByVal pImpuesto As Double, _
ByVal pTotal As Double, ByVal pFactorMoneda As Double, ByVal pConexionAdm As Object, _
Optional ByRef pNoError As Long = 0, Optional pMoverInventario As Boolean = True) As Boolean
    
    Dim RsVentasPosDetalles As Object, RsInventario As Object, _
    RsPartes As Object, RsMaDeposito As Object, MulTiplicador As Integer
    
    Dim mConexionAdm As Object ' New ADODB.Connection
    
    On Error GoTo Falla_Local
    
    EscribirComponentesProductos = False
    
    Set RsVentasPosDetalles = mClsRutinas.CrearAdoRs
    Set RsInventario = mClsRutinas.CrearAdoRs
    Set RsPartes = mClsRutinas.CrearAdoRs
    Set mConexionAdm = mClsRutinas.CrearAdoCn
    
    mConexionAdm.ConnectionString = pConexionAdm.ConnectionString
    mConexionAdm.Open
    
    RsPartes.CursorLocation = adUseClient
    
    RsPartes.Open _
    "SELECT * FROM MA_PARTES LEFT JOIN MA_PRODUCTOS " & _
    "ON MA_PRODUCTOS.C_CODIGO = MA_PARTES.C_PARTE " & _
    "WHERE MA_PARTES.C_CODIGO = '" & pCodigoProducto & "' ", _
    mConexionAdm, adOpenStatic, adLockReadOnly, adCmdText
    
    RsPartes.ActiveConnection = Nothing
    
    If Not RsPartes.EOF Then
        
        pPrecio = (pPrecio / RsPartes.RecordCount)
        pSubtotal = (pSubtotal / RsPartes.RecordCount)
        pDescuento = (pSubtotal / RsPartes.RecordCount)
        pImpuesto = (pImpuesto / RsPartes.RecordCount)
        pTotal = (pTotal / RsPartes.RecordCount)
        pCosto = (pCosto / RsPartes.RecordCount)
        
        While Not RsPartes.EOF
            
            pLinea = pLinea + 1
            
            mClsRutinas.Cerrar_Recordset RsVentasPosDetalles
            mClsRutinas.Cerrar_Recordset RsInventario
            
            RsInventario.Open _
            "SELECT * FROM TR_INVENTARIO " & _
            "WHERE c_Concepto = '" & pTipoMovimiento & "' " & _
            "AND c_Documento = '" & pDocumento & "' " & _
            "AND c_Deposito = '" & pDeposito & "' ", _
            pConexionAdm, adOpenStatic, adLockBatchOptimistic, adCmdText
            
            'COPIAR AL INVENTARIO
            
            RsInventario.AddNew
            RsInventario!c_Linea = pLinea
            RsInventario!c_Concepto = pTipoMovimiento
            RsInventario!c_Documento = pDocumento
            RsInventario!c_Deposito = pDeposito
            RsInventario!c_CodArticulo = RsPartes!c_Parte
            RsInventario!n_Cantidad = (RsPartes!n_Cantidad * pCantidadFactor)
            RsInventario!n_Costo = pCosto
            RsInventario!n_Precio = pPrecio
            
            Select Case pPrecioVenta
                Case 1
                    RsInventario!n_Precio_Original = RsPartes!N_Precio1
                Case 2
                    RsInventario!n_Precio_Original = RsPartes!N_Precio2
                Case 3
                    RsInventario!n_Precio_Original = RsPartes!N_Precio3
                Case Else
                    RsInventario!n_Precio_Original = RsPartes!N_Precio2
            End Select
            
            RsInventario!n_Subtotal = 0
            RsInventario!n_Impuesto = 0
            RsInventario!ns_Descuento = 0
            RsInventario!n_Total = 0
            RsInventario!c_TipoMov = IIf(pTipoMovimiento = "VEN", "Descargo", "Cargo")
            RsInventario!n_Cant_Teorica = 0
            RsInventario!n_Cant_Diferencia = 0
            RsInventario!f_Fecha = pFechaDocumento
            RsInventario!c_CodLocalidad = pLocalidad
            RsInventario!n_FactorCambio = pFactorMoneda
            RsInventario!c_descripcioN = RsPartes!c_Descri
            RsInventario!c_Compuesto = pCodigoProducto
            
            RsInventario.UpdateBatch
            
            'MOVER EL INVENTARIO
            If pMoverInventario Then
                
                mClsRutinas.Cerrar_Recordset RsMaDeposito
                RsMaDeposito.Open _
                "SELECT * FROM MA_DEPOPROD " & _
                "WHERE C_CODDEPOSITO = '" & pDeposito & "' " & _
                "AND C_CODARTICULO = '" & RsInventario!c_CodArticulo & "'" & _
                "  " & _
                "  " _
                , pConexionAdm, adOpenStatic, adLockBatchOptimistic, adCmdText
                
                If RsMaDeposito.EOF Then
                    RsMaDeposito.AddNew
                    RsMaDeposito!c_CodDeposito = CStr(Trim(pDeposito))
                    RsMaDeposito!c_descripcioN = CStr(Trim(RsInventario!c_descripcioN))
                    RsMaDeposito!c_CodArticulo = Trim(RsInventario!c_CodArticulo)
                End If
                
                If Trim(UCase(pTipoMovimiento)) = "VEN" Then
                    RsMaDeposito!n_Cantidad = Round(RsMaDeposito!n_Cantidad - RsInventario!n_Cantidad, 8)
                Else
                    RsMaDeposito!n_Cantidad = Round(RsMaDeposito!n_Cantidad + RsInventario!n_Cantidad, 8)
                End If
                
                RsMaDeposito!n_Cant_Comprometida = 0
                RsMaDeposito!n_Cant_Ordenada = 0
                
                RsMaDeposito.UpdateBatch
                
            End If
            
            RsPartes.MoveNext
            
        Wend
        
        EscribirComponentesProductos = True
        
    End If
    
    Exit Function
    
Falla_Local:
    
    pNoError = Err.Number
    'MsgBox Err.Description
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    GrabarArchivoErrores mErrorNumber, mErrorDesc, _
    "ACTUALIZANDO INVENTARIO DE PARTES DE COMPUESTOS (EscribirComponentesProductos)"
    
End Function

Public Function CriteriodeBusquedaMasiva_Interfaz()
    frm_CriteriodeBusquedasdeCambioMasivosdeProductos.Show vbModal
    CriteriodeBusquedaMasiva_Interfaz = frm_CriteriodeBusquedasdeCambioMasivosdeProductos.criterio
    Set frm_CriteriodeBusquedasdeCambioMasivosdeProductos = Nothing
End Function

Public Sub CambioPreciosEnProductosCompuestos()
    'SELECT  * FROM  TR_PRODUCCION WHERE  (C_FORMULA IN  (SELECT C_FORMULA  FROM TR_PRODUCCION  WHERE (C_CODPRODUCTO = '0000024152') AND (B_PRODUCIR = '0'))) AND (B_PRODUCIR = N'1')
End Sub

Public Function ProductoEnDeposito(pCodProducto, Optional pCodeposito = "")
    Dim MRS As New ADODB.Recordset
    SQL = "SELECT * From MA_DEPOPROD WHERE(c_codarticulo = '" & pCodProducto & "') and (c_coddeposito= '" & DPS_LOCAL & "') "
    MRS.Open SQL, ENT.BDD, adOpenForwardOnly, adLockReadOnly
'    'DEBUG.PRINT Sql
    If MRS.EOF Then
        ProductoEnDeposito = Array(0, 0, 0)
    Else
        ProductoEnDeposito = Array(CStr(MRS!n_Cantidad), CStr(MRS!n_Cant_Comprometida), CStr(MRS!n_Cant_Ordenada))
    End If
    MRS.Close
    Set MRS = Nothing
End Function
