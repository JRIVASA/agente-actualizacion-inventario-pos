Attribute VB_Name = "datos"
Global gRutinas         As New cls_Rutinas
Global gClsDatos        As Object
Global G_UserServer
Global G_Server
Global G_PassServer
Global G_BD_ADM As String
Global G_BD_POS As String
Global RepararCierres As Boolean
Global MantenerInfoCliente  As Boolean
Global CostoMultiMonedaTasaHistorica As Boolean
Global gCommandTimeOut As Long

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
    
Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Global CampoFactorMonedaProd As Boolean
Global CampoCostoRep As Boolean
Global CampoCostoPro As Boolean

Global CodMonPred           As String
Global DesMonPred           As String
Global SimMonPred           As String
Global FacMonPred           As Double
Global DecMonPred           As Integer

Global EstimacionINV        As Variant
Global EstimacionPV         As Variant

Global ExisteTablaHistoricoMonedas As Boolean
Global ExisteTablaHistoricoCostos   As Boolean

Global ConceptoVentasNoFiscales         As String
Global ConceptoDevolucionesNoFiscales   As String
Global DiscriminarDocumentoNoFiscal As Boolean

Global ManejaIGTF                       As Boolean

Public Function sGetIni(ByVal SIniFile As String, ByVal SSection As String, _
ByVal sKey As String, ByVal SDefault As String) As String
    
    Const ParamMaxLength = 10000
    
    Dim STemp As String * ParamMaxLength
    Dim NLength As Integer
    
    STemp = Space$(ParamMaxLength)
    
    NLength = GetPrivateProfileString(SSection, sKey, SDefault, STemp, ParamMaxLength, SIniFile)
    
    sGetIni = Left$(STemp, NLength)
    
End Function

Public Sub sWriteIni(ByVal SIniFile As String, ByVal SSection As String, _
ByVal sKey As String, ByVal sData As String)
    WritePrivateProfileString SSection, sKey, sData, SIniFile
End Sub

Public Function BuscarRsM(ByVal Cadena As String, Conexion As ADODB.Connection, Optional Tipodecursor As Integer = 1) As ADODB.Recordset
    Dim rsTemp As New ADODB.Recordset
    rsTemp.CursorLocation = adUseServer
    rsTemp.Open Cadena, Conexion, adOpenKeyset, IIf(Tipodecursor = 1, adLockPessimistic, adLockBatchOptimistic), adCmdText
    Set BuscarRsM = rsTemp
End Function

Public Function BuscarRs(ByVal Cadena As String, Conexion As ADODB.Connection) As ADODB.Recordset
    Dim rsTemp As New ADODB.Recordset
    rsTemp.CursorLocation = adUseServer
    rsTemp.Open Cadena, Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    'rsTemp.Open Cadena, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    'rsTemp.ActiveConnection = Nothing
    Set BuscarRs = rsTemp
End Function

Public Sub BorrarRs(ByVal Cadena As String, Conexion As ADODB.Connection)
    Dim rsTemp As New ADODB.Recordset
    rsTemp.CursorLocation = adUseServer
    rsTemp.Open Cadena, Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
End Sub

Public Sub AdicionarRs(ByVal Tabla As String, ByVal Campos As Variant, ByVal Datos As Variant, Conexion As ADODB.Connection)
    Dim rsTemp As New ADODB.Recordset
    rsTemp.CursorLocation = adUseServer
    rsTemp.Open Tabla, Conexion, adOpenStatic, adLockBatchOptimistic, adCmdTabla
    If UBound(Campos) = UBound(Datos) Then
        rsTemp.AddNew
        For I = 0 To UBound(Campos)
            rsTemp(Campos(I)) = Datos(I)
        Next
        rsTemp.UpdateBatch
    Else
    End If
End Sub

Public Sub ActualizarRs(ByVal criterio As String, ByVal Campos As Variant, ByVal Datos As Variant, Conexion As ADODB.Connection, Optional Tipodecursor As Integer = 1)
    Dim rsTemp As New ADODB.Recordset
    rsTemp.CursorLocation = adUseServer
    rsTemp.Open criterio, Conexion, adOpenStatic, IIf(Tipodecursor = 1, adLockPessimistic, adLockBatchOptimistic), adCmdText
    While Not rsTemp.EOF
        If UBound(Campos) = UBound(Datos) Then
            For I = 0 To UBound(Campos)
                rsTemp(Campos(I)) = Datos(I)
            Next
            rsTemp.UpdateBatch
        Else
        End If
        rsTemp.MoveNext
    Wend
End Sub

'Cuenta y devuelve el numero de registros en un recordset
Public Function ContarRegs(Rs As ADODB.Recordset) As Long
    
    Dim I As Integer
    
    If Rs.EOF Then
        ContarRegs = 0
        Exit Function
    End If
    
    Rs.MoveFirst
    I = 0
    
    While Not Rs.EOF
        Rs.MoveNext
        I = I + 1
    Wend
    
    ContarRegs = I
    Rs.MoveFirst
    
End Function

Sub Main()
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(51) & Chr(55) & Chr(73) & Chr(51) _
    & Chr(88) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(87) & "-" _
    & Chr(57) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(50) & Chr(48) & "_" & Chr(90)
    
    Dim M As New Cls_InvCierresPend
    Dim mEtapa As String
    
    On Error GoTo Errores
    
    If App.PrevInstance Then End
    
    If IniciarAgenteCierres And Not RepararCierres Then
        
        mEtapa = "Instanciando gClsDatos"
        
        Set gClsDatos = CreateObject("recsun.cls_datos")
        
        mEtapa = "Verificando Creaci�n / Existencia de Campos MA_CIERRExFACTURA (nu_Turno|du_FechaFactura)"
        CrearCampoTabla "MA_CIERRExFACTURA", "nu_Turno", "FLOAT", ENT.POS, "0"
        CrearCampoTabla "MA_CIERRExFACTURA", "du_FechaFactura", "DATETIME", ENT.POS, "'0:0'"
        
        mEtapa = "Cargando los datos de la moneda predeterminada."
        Cargar_Monedas
        
        mEtapa = "Cargando los datos de Reglas Comerciales."
        Cargar_Reglas_Comerciales
        
        mEtapa = "Verificando si existe la tabla de Historico de Monedas."
        ExisteTablaHistoricoMonedas = ExisteTablaV3("MA_HISTORICO_MONEDAS", ENT.BDD, , False)
        
        mEtapa = "Verificando si existe la tabla de Historico de Costos Precios."
        ExisteTablaHistoricoCostos = ExisteTablaV3("MA_HISTORICO_COSTO_PRECIO", ENT.BDD, , False)
        
        mEtapa = "Ejecutar Cierres Pendientes"
        
        M.EjecutarCierresPendientes
        
        Set gClsDatos = Nothing
        
    'ElseIf IniciarAgenteCierres And RepararCierres Then
    ElseIf RepararCierres Then
        mEtapa = "Ejecutar Reparacion Cierres"
        M.EjecutarReparacionCierres
    End If
    
    End
    
Errores:
    
    GrabarArchivoErrores Err.Number, Err.Description, "Main. " & mEtapa
    
    End
    
End Sub

Public Sub GrabarArchivoErrores(ByVal pNumero, ByVal pDescripcion, ByVal pProceso)
    
    Dim mCadenaError As String
    
    Open App.Path & "\AgentCierresError.log" For Append Access Write As #1
    mCadenaError = Now & " - " & "Error: " & pNumero & ", " & pDescripcion & ", Proceso: " & pProceso
    Print #1, mCadenaError
    Close #1
    
End Sub

Private Function IniciarAgenteCierres() As Boolean
    
    Dim Srv_Local As String, UserDb As String, UserPwd As String
    Dim BD_ADM As String, BD_POS As String
    Dim Provider_Local As String, Setup As String
    Dim NewUser As String, NewPassword As String
    Dim mCnADM As ADODB.Connection, mCnPOS As ADODB.Connection
    
    On Error GoTo Err_Open
    
    Setup = App.Path & "\Setup.ini"
    
    If Dir(Setup) = "" Then
        Setup = App.Path & "\UserLogon\Console.ini"
    End If
    
    gCommandTimeOut = -1
    
    Srv_Local = sGetIni(Setup, "Server", "Srv_Local", "")
    
    BD_ADM = sGetIni(Setup, "Server", "BD_ADM", "VAD10")
    BD_POS = sGetIni(Setup, "Server", "BD_POS", "VAD20")
    
    UserDb = sGetIni(Setup, "Server", "User", "SA")
    UserPwd = sGetIni(Setup, "Server", "Password", "")
    
    If Not (UCase(UserDb) = "SA" And Len(UserPwd) = 0) Then
        
        Dim mClsTmp As Object
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If Not mClsTmp Is Nothing Then
            UserDb = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, UserDb)
            UserPwd = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, UserPwd)
        End If
        
    End If
    
    G_Server = Srv_Local: G_UserServer = UserDb: G_PassServer = UserPwd
    
    Provider_Local = sGetIni(Setup, "Proveedor", "Proveedor", "?")
    RepararCierres = Val(sGetIni(Setup, "Entrada", "Reparar", "")) = 1
    MantenerInfoCliente = Val(sGetIni(Setup, "Entrada", "MantenerInfoCliente", "")) = 1
    CostoMultiMonedaTasaHistorica = Val(sGetIni(Setup, "Entrada", "CostoMultiMonedaTasaHistorica", "1")) = 1
    
    If IsNumeric(sGetIni(Setup, "Server", "CommandTimeOut", "")) Then
        gCommandTimeOut = sGetIni(Setup, "Server", "CommandTimeOut", "")
    End If
    
    If gCommandTimeOut > -1 Then
        ENT.POS.CommandTimeout = gCommandTimeOut
        ENT.BDD.CommandTimeout = gCommandTimeOut
    End If
    
    If Srv_Local <> "" And Provider_Local <> "" Then
        
        NewUser = "": NewPassword = ""
        
        If EstablecerConexion(mCnADM, Srv_Local, BD_ADM, , UserDb, UserPwd, True, NewUser, NewPassword) Then
            If NewUser <> "" Or NewPassword <> "" Then
                sWriteIni Setup, "Server", "User", NewUser
                sWriteIni Setup, "Server", "Password", NewPassword
            End If
        Else
            Err.Raise 999, , "Connection Error"
        End If
        
        NewUser = "": NewPassword = ""
        
        If EstablecerConexion(mCnPOS, Srv_Local, BD_POS, , UserDb, UserPwd, True, NewUser, NewPassword) Then
            If NewUser <> "" Or NewPassword <> "" Then
                sWriteIni Setup, "Server", "User", NewUser
                sWriteIni Setup, "Server", "Password", NewPassword
            End If
        Else
            Err.Raise 999, , "Connection Error"
        End If
        
        ENT.BDD.Open mCnADM.ConnectionString
        ENT.POS.Open mCnPOS.ConnectionString
        
        G_Server = Srv_Local: G_UserServer = UserDb: G_PassServer = UserPwd
        G_BD_ADM = BD_ADM: G_BD_POS = BD_POS
        
        'validar variables de concepto
        
        ConceptoVentasNoFiscales = sGetIni(Setup, "ENTRADA", "ConceptoVentasNoFiscales", Empty)
        ConceptoDevolucionesNoFiscales = sGetIni(Setup, "ENTRADA", "ConceptoDevolucionesNoFiscales", Empty)
        
        DiscriminarDocumentoNoFiscal = Val(sGetIni(Setup, "ENTRADA", "DiscriminarDocumentoNoFiscal", "0")) = 1
        ' , adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If DiscriminarDocumentoNoFiscal Then
            
            Dim TempRs As New ADODB.Recordset
            
            SQL = "SELECT * FROM " & G_BD_ADM & ".DBO.MA_CONCEPTOS " & _
            "WHERE IDConcepto = " & SVal(ConceptoVentasNoFiscales) & " " & _
            "AND IDProceso = 44 "
            
            TempRs.Open SQL, ENT.BDD, adOpenForwardOnly, adLockOptimistic, adCmdText
            
            If TempRs.EOF Then
                GrabarArchivoErrores -1, "La configuracion de la variable ConceptoVentasNoFiscales No es valida en el Archivo: " & Setup, "Verificacion de Variables de concepto"
                TempRs.Close
                Exit Function
            End If
            
            TempRs.Close
            
            SQL = "SELECT * FROM " & G_BD_ADM & ".DBO.MA_CONCEPTOS " & _
            "WHERE IDConcepto = " & SVal(ConceptoDevolucionesNoFiscales) & " " & _
            "AND IDProceso = 2 "
            
            TempRs.Open SQL, ENT.BDD, adOpenForwardOnly, adLockOptimistic, adCmdText
            
            If TempRs.EOF Then
                GrabarArchivoErrores -1, "La configuracion de la variable ConceptoDevolucionesNoFiscales No es valida en el Archivo: " & Setup, "Verificacion de Variables de concepto"
                TempRs.Close
                Exit Function
            End If
            
            TempRs.Close
            
        End If
        
        IniciarAgenteCierres = True
        
    Else
        GrabarArchivoErrores -1, "No se encontro la Configuracion, Archivo: " & Setup, "Abriendo Conexiones"
    End If
    
    Exit Function
    
Err_Open:
    
    GrabarArchivoErrores Err.Number, Err.Description, "Abriendo Conexiones"
    
End Function

'*********************************************** Funciones de Apoyo ****************************************************************

Public Function EstablecerConexion(pCn As ADODB.Connection, pServidor As String, ByVal pBD As String, _
Optional pCnnTOut As Long = 15, _
Optional ByRef pUser, Optional ByRef pPassword, Optional AuthReset As Boolean = False, _
Optional ByRef NewUser As String, Optional ByRef NewPassword As String) As Boolean
    
Retry:
    
    On Error GoTo ErrHandler
    
    Dim mCadena As String
    Dim mServidor As String
    
    Set pCn = New ADODB.Connection
    pCn.ConnectionTimeout = pCnnTOut
    
    If IsMissing(pUser) Or IsMissing(pPassword) Then
        pCn.Open CadenaConexion(pServidor, pBD, pCnnTOut)
    Else
        pCn.Open CadenaConexion(pServidor, pBD, pCnnTOut, CStr(pUser), CStr(pPassword))
    End If
    
    EstablecerConexion = True
    
    Exit Function
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If AuthReset Then
        
        If Err.Number = -2147217843 Then
            
            Dim mClsTmp As Object
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If mClsTmp Is Nothing Then GoTo UnhandledErr
            
            TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
            
            If Not IsEmpty(TmpVar) Then
                pUser = TmpVar(0): pPassword = TmpVar(1)
                NewUser = TmpVar(2): NewPassword = TmpVar(3)
                Resume Retry
            End If
            
            Set mClsTmp = Nothing
            
        End If
        
    End If
    
UnhandledErr:
    
    GrabarArchivoErrores mErrNumber, mErrDesc, "Abriendo Conexiones"
    
End Function

Private Function CadenaConexion(pServidor As String, pBD As String, Optional pCnnTOut As Long = 0, Optional ByVal pUser, Optional ByVal pPassword)
    
    Dim TempUser As String, TempPassword As String
    
    If IsMissing(pUser) Or IsMissing(pPassword) Then
        TempUser = G_UserServer: TempPassword = G_PassServer
    Else
        TempUser = CStr(pUser): TempPassword = (pPassword)
    End If
    
    CadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" & pBD & ";Data Source=" & IIf(Trim(pServidor) <> "", pServidor, G_Server) & ";" _
    & IIf(TempUser = vbNullString Or TempPassword = vbNullString, _
    "Persist Security Info=False;User ID=" & TempUser & ";", _
    "Persist Security Info=True;User ID=" & TempUser & ";Password=" & TempPassword & ";")
    
    'Debug.Print CadenaConexion
    
End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
            
            Case FBD_Fecha
                
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
                
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
                
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
                
                FechaBD = Format(Expression, "HH:mm")
                
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim MRS As ADODB.Recordset: Set MRS = New ADODB.Recordset
    
    MRS.Open SQL, IIf(pCn Is Nothing, ENT.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText
    
    If Not MRS.EOF Then
        BuscarValorBD = MRS.Fields(Campo).Value
    Else
        BuscarValorBD = pDefault
    End If
    
    MRS.Close
    
    Exit Function
    
Err_Campo:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    GrabarArchivoErrores mErrorNumber, mErrorDesc, "BuscarValorBD(). Consulta: " & vbNewLine & SQL & vbNewLine
    
    Err.Clear
    BuscarValorBD = pDefault
    
End Function

Public Function BuscarValorBD_Strict(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    Dim MRS As ADODB.Recordset: Set MRS = New ADODB.Recordset
    
    MRS.Open SQL, IIf(pCn Is Nothing, ENT.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText
    
    If Not MRS.EOF Then
        BuscarValorBD_Strict = MRS.Fields(Campo).Value
    Else
        BuscarValorBD_Strict = pDefault
    End If
    
    MRS.Close
    
    Exit Function
    
End Function

Public Function QuitarComillasDobles(ByVal pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(ByVal pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function LimitarComillasSimples(ByVal pCadena As String) As String
    
    Dim Cad As String
    
    Cad = pCadena
    
    Do While (Cad Like "*''*")
        Cad = Replace(Cad, "''", "'")
    Loop
    
    LimitarComillasSimples = Cad
    
End Function

Public Function FactorMonedaProducto(ByVal pCodigo As String, pCn As Object) As Double
    FactorMonedaProducto = BuscarValorBD("n_Factor", _
    "SELECT * FROM " & G_BD_ADM & ".DBO.MA_MONEDAS " & _
    "WHERE c_CodMoneda IN (SELECT TOP 1 c_CodMoneda " & _
    "FROM MA_PRODUCTOS WHERE c_Codigo = '" & QuitarComillasSimples(pCodigo) & "') ", 1, pCn)
End Function

Public Function FactorMonedaHistorico( _
ByVal pMoneda As String, ByVal pFecha As Date) As Variant
    
    If CDbl(pFecha) > 0 _
    And ExisteTablaV3("MA_HISTORICO_MONEDAS", ENT.BDD) Then ' Si hay historico y se paso fecha v�lida.
        
        mFactorDestino = CDec(BuscarValorBD("FactorEncontrado", _
        "SELECT TOP 1 isNULL(HIS.n_FactorPeriodo, MON.n_Factor) AS FactorEncontrado " & _
        "FROM MA_MONEDAS MON LEFT JOIN MA_HISTORICO_MONEDAS HIS " & _
        "ON MON.c_CodMoneda = HIS.c_CodMoneda " & _
        "WHERE MON.c_CodMoneda = '" & QuitarComillasSimples(pMoneda) & "' " & _
        "AND HIS.d_FechaCambioActual <= '" & FechaBD(pFecha, FBD_FULL) & "' " & _
        "ORDER BY HIS.d_FechaCambioActual DESC, HIS.ID DESC ", 1, ENT.BDD))
        
    Else
        
        mFactorDestino = CDec(BuscarValorBD("FactorEncontrado", _
        "SELECT TOP 1 MON.n_Factor AS FactorEncontrado " & _
        "FROM MA_MONEDAS MON " & _
        "WHERE MON.c_CodMoneda = '" & QuitarComillasSimples(pMoneda) & "' ", 1, ENT.BDD))
        
    End If
    
End Function

Public Function FactorHistoricoMonedaProducto( _
ByVal pCodigo As String, ByVal pFecha As Date, pCn As Object, _
Optional pMonedaPred As String, _
Optional pBuscarTasaHistorica As Boolean = True _
) As Variant
    
    Dim mMonedaProd As String
    
    For I = 1 To 5
        
        mMonedaProd = BuscarValorBD_Strict("c_CodMoneda", _
        "SELECT TOP 1 c_CodMoneda " & _
        "FROM MA_PRODUCTOS " & _
        "WHERE c_Codigo = '" & QuitarComillasSimples(pCodigo) & "' ", , ENT.POS)
        
        If Trim(mMonedaProd) <> Empty Then
            Exit For
        End If
        
        Sleep 200
        
    Next I
    
    If Trim(mMonedaProd) = Empty Then
        GrabarArchivoErrores 999, "No se pudo obtener la moneda del producto " & pCodigo & _
        " en BD POS (" & G_BD_POS & "), o es inv�lida. Terminando abruptamente para asegurar integridad de datos.", _
        "FactorHistoricoMonedaProducto()"
        End
    End If
    
    If UCase(mMonedaProd) <> UCase(pMonedaPred) Then
        
        If CDbl(pFecha) > 0 _
        And pBuscarTasaHistorica _
        And ExisteTablaHistoricoMonedas Then ' Si hay historico y se paso fecha v�lida.
            
            FactorHistoricoMonedaProducto = CDec(BuscarValorBD_Strict("FactorEncontrado", _
            "SELECT TOP 1 isNULL(HIS.n_FactorPeriodo, MON.n_Factor) AS FactorEncontrado " & _
            "FROM MA_MONEDAS MON LEFT JOIN MA_HISTORICO_MONEDAS HIS " & _
            "ON MON.c_CodMoneda = HIS.c_CodMoneda " & _
            "AND HIS.d_FechaCambioActual <= '" & FechaBD(pFecha, FBD_FULL) & "' " & _
            "WHERE MON.c_CodMoneda = '" & QuitarComillasSimples(mMonedaProd) & "' " & _
            "ORDER BY HIS.d_FechaCambioActual DESC, HIS.ID DESC", 1, ENT.BDD))
            
        Else
            
            FactorHistoricoMonedaProducto = CDec(BuscarValorBD_Strict("FactorEncontrado", _
            "SELECT TOP 1 MON.n_Factor AS FactorEncontrado " & _
            "FROM MA_MONEDAS MON " & _
            "WHERE MON.c_CodMoneda = '" & QuitarComillasSimples(mMonedaProd) & "' ", 1, ENT.BDD))
            
        End If
        
    Else
        FactorHistoricoMonedaProducto = CDec(1)
    End If
    
End Function

Public Function CostosProducto(ByVal pCodigo As String, pCn As Object) As Variant
    
    Dim pRsCostos As ADODB.Recordset
    
    Set pRsCostos = New ADODB.Recordset
    
    pRsCostos.CursorLocation = adUseClient
    
    pRsCostos.Open _
    "SELECT n_CostoAct, n_CostoAnt, n_CostoPro, n_CostoRep " & _
    "FROM MA_PRODUCTOS " & _
    "WHERE c_Codigo = '" & pCodigo & "' ", _
    pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not pRsCostos.EOF Then
        CostosProducto = Array( _
        CDec(pRsCostos!n_CostoAct), _
        CDec(pRsCostos!n_CostoAnt), _
        CDec(pRsCostos!n_CostoPro), _
        CDec(pRsCostos!n_CostoRep) _
        )
    Else
        CostosProducto = Array(CDec(0), CDec(0), CDec(0), CDec(0))
    End If
    
    pRsCostos.Close
    
End Function

Public Function ExisteCampoTabla(pCampo As String, Optional pRs, Optional pSql As String = "", _
Optional pValidarFecha As Boolean = False, Optional pCn) As Boolean
    
    Dim mAux As Variant
    Dim MRS As New ADODB.Recordset
    
    On Error GoTo Error
    
    If Not IsMissing(pRs) Then
        
        mAux = pRs.Fields(pCampo).Name
        
        If pValidarFecha Then
            mAux = pRs.Fields(pCampo).Value
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
        
    Else
        
        If IsMissing(pCn) Then
            MRS.Open pSql, ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        Else
            MRS.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        End If
        
        ExisteCampoTabla = True
        
        MRS.Close
        
    End If
    
    Exit Function
    
Error:
    
    'MsgBox Err.Description
    'Debug.Print Err.Description
    
    Err.Clear
    ExisteCampoTabla = False
    
End Function

Public Function CrearCampoTabla(ByVal pTabla As String, ByVal pCampo As String, _
ByVal pTipoDato As String, pCn As ADODB.Connection, _
Optional ByVal pValorDefault As String = Empty, _
Optional ByVal pPermiteNULL As Boolean = False, _
Optional ByVal pIdDefault As String = "*") As Boolean
    
    On Error Resume Next
    
    Dim mBD As String: mBD = IIf(IsEmpty(pBD), Empty, pBD & ".")
    
    Dim SQL As String: SQL = _
    "IF NOT EXISTS(SELECT * FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" & pTabla & "' AND COLUMN_NAME = '" & pCampo & "')" & vbNewLine & _
    "BEGIN" & vbNewLine & _
    "   ALTER TABLE dbo." & pTabla & " ADD " & pCampo & " " & pTipoDato & " " & _
    IIf(pPermiteNULL, "NULL", "NOT NULL") & IIf(pIdDefault = "*", _
    " " & "CONSTRAINT" & " " & pTabla & "_DEFAULT_" & pCampo, IIf(IsEmpty(pIdDefault), Empty, " " & pIdDefault)) & _
    IIf(IsEmpty(pValorDefault), Empty, " " & "DEFAULT" & " " & pValorDefault) & vbNewLine & _
    "   ALTER TABLE dbo." & pTabla & " " & "SET (LOCK_ESCALATION = TABLE)" & vbNewLine & _
    "   PRINT 'TABLA ACTUALIZADA'" & vbNewLine & _
    "END"
    
    pCn.Execute SQL, Success
    
    If Err.Number = 0 Then CrearCampoTabla = True
    
End Function

Public Function ExisteTablaV3(ByVal pTabla As String, _
pCn As Variant, _
Optional ByVal pBD As String = "", _
Optional pControlarError As Boolean = True) As Boolean
    ' pCn > Variant / Object / ADODB.Connection
    If pControlarError Then On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTablaV3 = Not (pRs.EOF And pRs.BOF)
    pRs.Close
End Function

Public Function EndOfDay(Optional ByVal pFecha) As Date
    If IsMissing(pFecha) Then pFecha = Now
    pFecha = SDate(CDate(pFecha))
    EndOfDay = DateAdd("h", 23, DateAdd("n", 59, DateAdd("s", 59, pFecha)))
End Function

Public Function ExecuteSafeSQL(ByVal SQL As String, _
pCn As ADODB.Connection, _
Optional ByRef Records = 0, _
Optional ByVal ReturnsResultSet As Boolean = False, _
Optional ByVal DisconnectedRS As Boolean = False) As ADODB.Recordset
    
    On Error GoTo Error
    
    If ReturnsResultSet Then
        If DisconnectedRS Then
            Set ExecuteSafeSQL = New ADODB.Recordset
            ExecuteSafeSQL.CursorLocation = adUseClient
            ExecuteSafeSQL.Open SQL, pCn, adOpenStatic, adLockReadOnly
            Set ExecuteSafeSQL.ActiveConnection = Nothing
        Else
            Set ExecuteSafeSQL = pCn.Execute(SQL, Records)
        End If
    Else
        pCn.Execute SQL, Records
        Set ExecuteSafeSQL = Nothing
    End If
    
    Exit Function
    
Error:
    
    Set ExecuteSafeSQL = Nothing
    Records = -1
    
End Function

' Formatea Fecha Corta o Larga si incluye hora, seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function GDate(ByVal pDate) As String: On Error Resume Next: GDate = FormatDateTime(pDate, vbGeneralDate): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function SDate(ByVal pDate) As String: On Error Resume Next: SDate = FormatDateTime(pDate, vbShortDate): End Function

' Formatea Hora Corta o Larga si incluye hora, seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function GTime(ByVal pDate) As String: On Error Resume Next: GTime = FormatDateTime(pDate, vbLongTime): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function STime(ByVal pDate) As String: On Error Resume Next: STime = FormatDateTime(pDate, vbShortTime): End Function

Public Sub Cargar_Monedas()
    
    On Error GoTo Error
    
    Dim RsEureka As New ADODB.Recordset
    
    RsEureka.CursorLocation = adUseClient
    
    RsEureka.Open "SELECT * FROM MA_MONEDAS WHERE b_Preferencia = 1", _
    ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set RsEureka.ActiveConnection = Nothing
    
    If Not RsEureka.EOF Then
        
        If RsEureka!b_preferencia Then
            
            Moneda_Des = RsEureka!c_descripcioN
            Moneda_Local = IIf(RsEureka!b_preferencia = True, True, False)
            Moneda_Cod = RsEureka!C_CODMONEDA
            Moneda_Fac = RsEureka!N_FACTOR
            Moneda_Simbolo = RsEureka!c_simbolo
            
            Std_Decm = RsEureka!n_decimales
            
            'Nuevas variables cuyo valor se asegura que nunca cambien
            'en ningun otro lado. Las variables anteriores a veces las usan para
            'monedas alternas con la funcion Buscar_Moneda. Estas nuevas propiedades
            'siempre se deben referir unicamente a los datos de la moneda predeterminada
            'y no deben ser alteradas en ninguna otra parte o metodo.
            
            CodMonPred = Moneda_Cod
            DesMonPred = Moneda_Des
            SimMonPred = Moneda_Simbolo
            FacMonPred = Moneda_Fac
            DecMonPred = Std_Decm
            
        End If
        
    Else
        
Error:
        
        If Err.Number <> 0 Then
            mErrorNumber = Err.Number
            mErrorDesc = Err.Description
            mErrorSource = "Cargar_Monedas() - " & Err.Source
        Else
            mErrorNumber = 999
            mErrorDesc = "Ejecucion Actual detenida por validaci�n. No se encontraron datos de la moneda predeterminada."
            mErrorSource = "Cargar_Monedas()"
        End If
        
        GrabarArchivoErrores mErrorNumber, mErrorDesc, mErrorSource
        
        End
        
    End If
    
    RsEureka.Close
    
End Sub

Public Sub Cargar_Reglas_Comerciales()
    
    On Error GoTo Error
    
    Dim RsEureka As New ADODB.Recordset
    
    RsEureka.CursorLocation = adUseClient
    
    RsEureka.Open "SELECT * FROM REGLAS_COMERCIALES", _
    ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set RsEureka.ActiveConnection = Nothing
    
    If Not RsEureka.EOF Then
        
        EstimacionINV = RsEureka!Estimacion_INV
        EstimacionPV = RsEureka!Estimacion_PV
        
    Else
        
Error:
        
        If Err.Number <> 0 Then
            mErrorNumber = Err.Number
            mErrorDesc = Err.Description
            mErrorSource = "Cargar_Reglas_Comerciales() - " & Err.Source
        Else
            mErrorNumber = 999
            mErrorDesc = "Ejecucion Actual detenida por validaci�n. No se encontraron datos de Reglas Comerciales."
            mErrorSource = "Cargar_Reglas_Comerciales()"
        End If
        
        GrabarArchivoErrores mErrorNumber, mErrorDesc, mErrorSource
        
        End
        
    End If
    
    RsEureka.Close
    
End Sub

Public Function BuscarReglaNegocioStr(CnAdm As ADODB.Connection, ByVal pCampo, _
Optional ByVal pDefault As String) As String
    
    Dim MRS As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_REGLASDENEGOCIO " & _
    "WHERE Campo = '" & pCampo & "' "
    
    MRS.Open mSQL, CnAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not MRS.EOF Then
        BuscarReglaNegocioStr = CStr(MRS!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    Exit Function
    
Errores:
    
    Err.Clear
    
    BuscarReglaNegocioStr = pDefault
    
End Function

' Safe Val() - Sin el problema de que Val no soporta numeros formateados.
' Ej: Val("10,000.00") = 10 | SVal("10,000.00") = 10000 ' Como debe ser.
Public Function SVal(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 0) As Double
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SVal = CDbl(pExpression)
    Else
        SVal = pDefaultValue
    End If
End Function

' Min Val()
Public Function MinVal(ByVal pExpression As Variant, _
Optional ByVal pMinVal As Double = 0) As Double
    MinVal = SVal(pExpression, pMinVal)
    MinVal = IIf(MinVal < pMinVal, pMinVal, MinVal)
End Function

' Max Val()
Public Function MaxVal(ByVal pExpression As Variant, _
Optional ByVal pMaxVal As Double = 0) As Double
    MaxVal = SVal(pExpression, MaxVal)
    MaxVal = IIf(MaxVal > pMaxVal, pMaxVal, MaxVal)
End Function

' Min Val > 0
Public Function MinVal0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 1) As Double
    MinVal0 = SVal(pExpression, pDefaultValue)
    MinVal0 = IIf(MinVal0 > 0, MinVal0, pDefaultValue)
End Function

Public Function SDec(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 0) As Variant
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SDec = CDec(pExpression)
    Else
        SDec = CDec(pDefaultValue)
    End If
End Function

' Min Decimal()
Public Function MinDec(ByVal pExpression As Variant, _
Optional ByVal pMinDec As Variant = 0) As Variant
    MinDec = SDec(pExpression, pMinDec)
    MinDec = IIf(MinDec < pMinDec, pMinDec, MinDec)
End Function

' Max Decimal()
Public Function MaxDec(ByVal pExpression As Variant, _
Optional ByVal pMaxDec As Variant = 0) As Variant
    MaxDec = SDec(pExpression, MaxDec)
    MaxDec = IIf(MaxDec > pMaxDec, pMaxDec, MaxDec)
End Function

' Min Decimal > 0
Public Function MinDec0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 1) As Variant
    MinDec0 = SDec(pExpression, pDefaultValue)
    MinDec0 = IIf(MinDec0 > 0, MinDec0, pDefaultValue)
End Function
